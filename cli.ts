/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GGraphRenderer from './class/GGraphRenderer';
import GGraph from './class/GGraph';
import GEdge from './class/GEdge';

console.log('Hello.');

let graph = {
	nodes: [
		{
			id: 'r',
			label: "Red",
			color: "#d64",
			x: 150,
			y: 200,
		},
		{
			id: 'g',
			label: "Green",
			color: "#4c2",
			x: 450,
			y: 200,
		},
		{
			id: 'b',
			label: "Blue",
			color: "#66d",
			x: 300,
			y: 375,
		}
	],
	edges: [
		{
			start: 'r',
			end:   'g'
		},
		{
			start: 'g',
			end:   'b',
		},
		{
			start: 'b',
			end:   'r',
		}
	]
};


// Graph
let g = new GGraph();
graph.nodes.map(function(n) {
	g.addNode(n.id, n);
});
let e_id = 1;
graph.edges.map(function(e) {
	g.addEdge((<any>e).id || 'e' + e_id++, e, g.getNodeById(e.start), g.getNodeById(e.end));
});

// Add Clock demo graph
let cx = 700;
let cy = 300;
let r = 100;
let cn = g.addNode('s', { x: cx, y: cy });

let h = 12;
for (let a = 0; a < 2 * Math.PI; a += Math.PI / 6) {
	let color = "hsl(" + (180. / Math.PI * a) + ",100%,40%)";
	let rn = g.addNode(undefined, { color: color, label: h, x: cx + r * Math.sin(a), y: cy - r * Math.cos(a) });
	let e = g.addEdge(undefined, { color: color }, cn, rn);
	h = (h + 1) % 12;
}

// Initialize D3.js
declare let d3: any;
let svg = d3.select('svg');
svg.style('visibility', 'visible');

// Graph Renderer
let sl = new GShapeLibrary();
let gr = new GGraphRenderer(g, sl, svg);
gr.render();

console.log('Bye.');

