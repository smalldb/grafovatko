/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import typescript from 'rollup-plugin-typescript2';
import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import json from 'rollup-plugin-json';


let version = require('./build/version.json').version;

let banner = '/*!\n * Grafovatko ' + version + '\n'
	+ ' * Copyright (c) 2017  Josef Kufner <josef@kufner.cz>\n'
	+ ' * https://grafovatko.smalldb.org/\n'
	+ ' * @license Apache-2.0\n'
	+ ' *\n'
	+ ' * Bundled libraries:\n'
	+ ' *   - Dagre: MIT License, https://github.com/cpettitt/dagre\n'
	+ ' *   - svg-intersections: BSD License, https://github.com/signavio/svg-intersections\n'
	+ ' *\n'
	+ ' */';

// rollup.config.js
export default {
	input: 'build/Grafovatko.index.js',
	output: [
		{
			file: 'build/grafovatko.js',
			format: 'umd',
			name: 'G',
			banner: banner,
			sourcemap: true,
		}
	],
	plugins: [
		
		nodeResolve({
			jsnext: true,
			main: true,
			browser: true,
		}),

		typescript({
			typescript: require('typescript'),
			importHelpers: true,
			removeComments: true,
			alwaysStrict: true,
			noEmitOnError: true,
			declaration: true,
			cache: false,
			sourceMap: true,
		}),

		commonjs({
			sourceMap: true
		}),

		json({
		})

	]
};

