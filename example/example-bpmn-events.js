// Graph View
let gv = new G.GraphView('svg');
let g = gv.graph;

let columns = [
	[ 'Top-level',           'start', false, false ],
	[ 'Subprocess non-int.', 'start', false, false ],
	[ 'Subprocess int.',     'start', true,  false ],

	[ 'Catching',          'intermediate', false, false ],
	[ 'Boundary non-int.', 'intermediate', false, false ],
	[ 'Boundary int.',     'intermediate', true,  false ],
	[ 'Throwing',          'intermediate', false, true ],

	[ 'End', 'end', false, false ],
];

let symbols = [
	[ 'none',              'x.....xx' ],
	[ 'message',           'xxxxxxxx' ],
	[ 'timer',             'xxxxxx..' ],
	[ 'error',             '.x..x..x' ],
	[ 'escalation',        '.xx.xxxx' ],
	[ 'cancel',            '....x..x' ],
	[ 'compensation',      '.x..x.xx' ],
	[ 'conditional',       'xxxxxx..' ],
	[ 'link',              '...x..x.' ],
	[ 'signal',            'xxxxxxxx' ],
	[ 'terminate',         '.......x' ],
	[ 'multiple',          'xxxxxxxx' ],
	[ 'parallel_multiple', 'xxxxxx..' ],
];

let row_height = 75;
let col_width = 75;

for (let c = 0; c < columns.length; c++) {
	let col = columns[c];
	g.addNode('col_' + c, {
		x: c * col_width,
		y: - row_height + row_height / 3 * (c % 2),
		shape: "label",
		label: col[0],
	});
}

for (let r = 0; r < symbols.length; r++) {
	let sym = symbols[r];

	g.addNode('row_' + r, {
		x: - 1.5 * col_width,
		y: r *  row_height,
		shape: "label",
		label: sym[0],
	});
	
	for (let c = 0; c < columns.length; c++) {
		let col = columns[c];

		g.addNode('ev_' + r + '_'+ c, {
			x: c * col_width,
			y: r * row_height,
			shape: "bpmn.event",
			color: sym[1][c] == 'x' ? '#000' : '#eee',
			event_type: col[1],
			event_symbol: sym[0],
			event_is_interrupting: col[2],
			event_is_throwing: col[3],
		});
	}
}

console.log('Done.');

