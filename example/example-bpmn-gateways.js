// Graph View
let gv = new G.GraphView('svg');
let g = gv.graph;

let symbols = [
	[ 'Exclusive:',            [ 'none',  null,        'exclusive'      ]],
	[ 'Event-Based:',          [ 'event', null,        'event'          ]],
	[ 'Parallel Event-Based:', [ null,    null,        'parallel_event' ]],
	[ 'Inclusive:',            [ null,    'inclusive', null             ]],
	[ 'Complex:',              [ null,    'complex',   null             ]],
	[ 'Parallel:',             [ null,    'parallel',  null             ]],
];

let row_height = 75;
let col_width = 75;

for (let r = 0; r < symbols.length; r++) {
	let sym = symbols[r];

	g.addNode('label_' + r, {
		x: -1.5 * col_width,
		y: r * row_height,
		shape: "label",
		label: sym[0],
	});

	for (let c = 0; c < sym[1].length; c++) {
		let col = sym[1][c];

		if (col === null) {
			continue;
		}

		g.addNode('gw_' + r + '_'+ c, {
			x: c * col_width,
			y: r * row_height,
			shape: "bpmn.gateway",
			gateway_type: col,
			gateway_instantiate: c >= 2,
		});
	}
}

console.log('Done.');

