let nodes_count = 8;
let f = 'rgba(255, 255, 255, 0.6)';
let graph_data = {
	layout: 'row',
	layoutOptions: {
		sortNodes: true,
	},
	nodes: [
		{ id: 'C', fill: '#afa', graph: { layout: 'column', nodes: [ { id: 'C1', fill: f }, { id: 'C2', fill: f }, { id: 'C3', fill: f } ] }},
		{ id: 'F', fill: '#faf', graph: { layout: 'column', nodes: [ { id: 'F1', fill: f }, { id: 'F2', fill: f }, { id: 'F3', fill: f } ] }},
		{ id: 'E', fill: '#aaf', graph: { layout: 'column', nodes: [ { id: 'E1', fill: f }, { id: 'E2', fill: f }, { id: 'E3', fill: f } ] }},
		{ id: 'A', fill: '#faa', graph: { layout: 'column', nodes: [ { id: 'A1', fill: f }, { id: 'A2', fill: f }, { id: 'A3', fill: f } ] }},
		{ id: 'B', fill: '#ffa', graph: { layout: 'column', nodes: [ { id: 'B1', fill: f }, { id: 'B2', fill: f }, { id: 'B3', fill: f } ] }},
		{ id: 'D', fill: '#aff', graph: { layout: 'column', nodes: [ { id: 'D1', fill: f }, { id: 'D2', fill: f }, { id: 'D3', fill: f } ] }},
	],
	edges: [
		{ start: 'A2', end: 'B2' },
		{ start: 'A1', end: 'C2' },
		{ start: 'A3', end: 'C2' },
		{ start: 'B1', end: 'C1' },
		{ start: 'B2', end: 'C2' },
		{ start: 'C1', end: 'B2' },
		{ start: 'C1', end: 'D2' },
		{ start: 'C3', end: 'B2' },
		{ start: 'C3', end: 'D2' },
		{ start: 'B3', end: 'C3' },
		{ start: 'C2', end: 'D2' },
		{ start: 'C2', end: 'E1' },
		{ start: 'C2', end: 'E3' },
		{ start: 'C1', end: 'D1' },
		{ start: 'C3', end: 'D3' },
		{ start: 'D2', end: 'E2' },
		{ start: 'D1', end: 'F2' },
		{ start: 'D3', end: 'F2' },
	],
};

// Generate few additional boring nodes
for (var i = graph_data.nodes.length; i < nodes_count; i++) {
	var n = String.fromCharCode(65 + i);
	var p = String.fromCharCode(65 + i - 1);
	graph_data.nodes.push({
		id: n,
		fill: '#eee',
		graph: { layout: 'column', nodes: [
			{ id: n + '1', fill: f },
			{ id: n + '2', fill: f },
			{ id: n + '3', fill: f }
		] }
	});
	graph_data.edges.push({
		start: p + '2',
		end: n + '2',
	});
}

let gv = new G.GraphView('svg', graph_data);

console.log('Done.');

