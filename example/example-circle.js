// Build demo graph
let g = new G.Graph();
let cx = 800;
let cy = 350;
let r = 250;
let cn = g.addNode('s', { x: cx, y: cy, label: 'G.☺', shape: 'hexagon' });

// Demo graph: Inner circle
let h = 12;
for (let a = 0; a < 2 * Math.PI; a += Math.PI / 6) {
	let color = "hsl(" + (180. / Math.PI * a) + ",100%,40%)";
	let rn = g.addNode(undefined, { color: color, label: h, 'class': 'inner', shape: h % 3 == 0 ? 'diamond' : 'circle', x: cx + 0.6 * r * Math.sin(a), y: cy - 0.6 * r * Math.cos(a) });
	let e = g.addEdge(undefined, { color: color, arrowHead: 'vee' }, cn, rn);
	h = (h + 1) % 12;
}

// Demo graph: Outter circle
h = 12;
for (let a = Math.PI / 12; a < 2 * Math.PI; a += Math.PI / 6) {
	let color = "hsl(" + (180. / Math.PI * a) + ",100%,40%)";
	let rn = g.addNode(undefined, { color: color, label: h + "½", 'class': 'outter', shape: 'round', x: cx + r * Math.sin(a), y: cy - r * Math.cos(a) });
	let e = g.addEdge(undefined, { color: color, arrowHead: 'onormal-diamond-oinv' }, cn, rn);
	h++;
}

// Graph View
let gv = new G.GraphView('svg', g);

console.log('Done.');

