#!/usr/bin/env php
<?php

if (php_sapi_name() !== 'cli') {
	die('CLI only.');
}

function list_dir(& $nodes, & $edges, $dir, $node_id, $indent = "") {

	$d = basename($dir);
	$nodes[] = [ 'id' => $node_id, 'label' => $d ];

	foreach (scandir($dir) as $i => $f) {
		if ($f[0] == '.') {
			continue;
		}
		$fn = "$dir/$f";
		$child_id = $node_id.'.'.$i;

		//echo "$indent$d -> $f\n";

		if (is_dir($fn) && !is_link($fn)) {
			list_dir($nodes, $edges, $fn, $child_id, $indent."  ");
			$edges[] = [ 'start' => $node_id, 'end' => $child_id ];
		} else {
			//$nodes[] = [ 'id' => $child_id, 'label' => $f ];
		}
	}
}

$nodes = [];
$edges = [];
list_dir($nodes, $edges, dirname(__DIR__), '0');

ob_start();

echo "// Input data (generated)\n";
echo "let graph = ", json_encode([
	'nodes' => $nodes,
	'edges' => $edges,
	'layout' => 'dagre',
	'layoutOptions' => [
		'rankdir' => 'LR',
	],
]), ";\n";

echo <<<eof

// Graph View
let gv = new G.GraphView('svg', graph);

console.log('Done.');

eof;

file_put_contents(__DIR__.'/example-filesystem.js', ob_get_clean());

