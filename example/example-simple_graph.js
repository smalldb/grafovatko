let graph_data = {
	nodes: [
		{ id: 'first', x:  50, y: 50, shape: 'hexagon' },
		{ id: 'second', x: 200, y: 100, shape: 'round' },
		{ id: 'third', x: 100, y: 200, shape: 'round', label: '3rd', fill: '#ddf' },
	],
	edges: [
		{ id: 'ln', start: 'first', end: 'second', color: '#44f', stroke_dasharray: [5, 3] },
		{ id: 'p1', start: 'first', end: 'second', color: '#d46', points: [[175, 20]] },
		{ id: 'p2', start: 'first', end: 'second', color: '#4d4', points: [[50, 150]] },
		{ id: 'p3', start: 'second', end: 'third', color: '#ddd' },
	]
};
let gv = new G.GraphView('svg', graph_data);

console.log('Done.');

