// Graph View
let gv = new G.GraphView('svg');
gv.graph.setLayout('random');

// Generate a lot of nodes
let random_count = 1000;
console.log('Generating %d random nodes...', random_count);
for (let i = 0; i < random_count; i++) {
	gv.graph.addNode(i, {
		id: i,
		label: i,
		color: "#888",
		fill: "hsl(" + 360 * Math.random() + ",90%,90%)",
	});
}
console.log('Generating %d random nodes... Done.', random_count);

console.log('Done.');

