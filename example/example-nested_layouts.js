var linear_options = {
	spacing: 2
};

function linear_nodes(prefix, n) {
	var colors = [ '#ccc', '#fcc', '#ffc', '#fff', '#cff', '#ccf' ];
	var labels = [ 'q₁',   'q₂',   'q₃',   'q₄',   'q₅',   'q₅' ];
	var nodes = [];

	if (n === undefined || n > 6) {
		n = 4;
	}

	for (var i = 0; i < n; i++) {
		nodes.push({
			id: prefix + 'q' + (i + 1),
			shape: 'round',
			label: labels[i],
			fill: colors[i],
			x: 10 * i, y: 10 * i,
		});
	}

	return nodes;
}

function linear_edges(prefix, n) {
	if (n === undefined || n > 6) {
		n = 4;
	}

	var q1 = 'q1';
	var q2 = 'q2';
	var q3 = 'q' + (n - 1);
	var q4 = 'q' + n;

	var edges = [
		{ start: prefix + q1, end: prefix + q4, color: '#ccc' },
		{ start: prefix + q1, end: prefix + q2, color: '#d46' },
		{ start: prefix + q3, end: prefix + q4, color: '#44d' },
		{ start: prefix + q1, end: prefix + q3, color: '#888' },
		{ start: prefix + q4, end: prefix + q2, color: '#888' },
	];

	for (var i = 1; i < n - 2; i++) {
		edges.push({
			start: prefix + 'q' + (i + 1),
			end: prefix + 'q' + (i + 2),
			color: '#4d4',
		});
	}
	return edges;
}

var graph_data = {
	nodes: [
		{
			id: 'root',
			label: '"row" layout',
			color: '#aaa',
			x: 0,
			y: 0,
			graph: {
				layout: 'row',
				layoutOptions: {
					align: -1, // or 'top'
				},
				nodes: [
					{
						id: 'A',
						label: '"column" layout',
						color: '#888',
						fill: '#efe',
						graph: {
							layout: 'column',
							layoutOptions: {
								align: 1, // or 'right'
							},
							nodes: [
								{
									id: 'A.A.A',
									label: '"row" layout',
									fill: '#fff',
									graph: {
										layout: 'row',
										layoutOptions: linear_options,
										nodes: linear_nodes('A.A.A.', 4),
										edges: linear_edges('A.A.A.', 4),
									}
								}, {
									id: 'A.A.B',
									label: '"row" layout',
									fill: '#fff',
									graph: {
										layout: 'row',
										layoutOptions: linear_options,
										nodes: linear_nodes('A.A.B.', 5),
										edges: linear_edges('A.A.B.', 5),
									}
								}, {
									id: 'A.A.C',
									label: '"row" layout',
									fill: '#fff',
									graph: {
										layout: 'row',
										layoutOptions: linear_options,
										nodes: linear_nodes('A.A.C.', 6),
										edges: linear_edges('A.A.C.', 6),
									}
								}
							],
							edges: [],
						}
					}, {
						id: 'B',
						label: '"column" layout',
						graph: {
							layout: 'column',
							layoutOptions: linear_options,
							nodes: linear_nodes('B.', 5),
							edges: linear_edges('B.', 5),
						}
					}
				],
				edges: [],
			}
		},
	],
	edges: [],
};

console.log('Graph data:', graph_data);

let gv = new G.GraphView('svg', graph_data);

console.log('Done.');

