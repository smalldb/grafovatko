// Input data
let graph = {
	nodes: [
		{ id: "begin",  x: 0, y:   0, label: "Begin",  shape: "uml.initial_state" },
		{ id: "exists", x: 0, y: 100, label: "Exists", shape: "uml.state" },
		{ id: "end",    x: 0, y: 200, label: "End",    shape: "uml.final_state" },
	],
	edges: [
		{ start: "begin",  end: "exists", label: "Create" },
		{ start: "exists", end: "exists", label: "Edit"   },
		{ start: "exists", end: "end",    label: "Delete" },
	]
};

// Graph View
let gv = new G.GraphView('svg', graph);
gv.graph.setLayout('dagre');

console.log('SVG Element:', gv.svgElement);
console.log('Graph:', gv.graph);

console.log('Done.');

