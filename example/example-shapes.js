// Graph View
let gv = new G.GraphView('svg');
let g = gv.graph;

// Build graph
let col = 0;
let n = 0;
let step_x = 275;
let step_y = 175;
let offset_y = 60;
let grid_color = '#ddd';
let label_color = '#aaa';

for (let li = 0; li < gv.shapeLibraries.length; li++) {
	let lib = gv.shapeLibraries[li];
	let shapes = lib.listShapes();
	let row = 0;
	let old_prefix = null;
	//console.log("Library:", lib.constructor.name);

	for (let si = 0; si < shapes.length; si++) {
		let shape = shapes[si];

		let prefix = shape.replace(/\.[^.]*$/, '');
		if (old_prefix && prefix != old_prefix && prefix != shape) {
			row = 0;
			col++;
		}

		let id = 'shape_' + li + '_' + si;
		let x = step_x * col;
		let y = step_y * row;

		//console.log("\tShape:", shape);
		let l = g.addNode('label_' + id, { "x": x, "y": y, "shape": "label", "label": shape + ':', color: label_color });
		let n = g.addNode(id, { "x": x, "y": y + offset_y, "shape": shape, "label": shape });

		// grid - connector test
		/*
		let conn = [
			g.addNode('conn_nw_' + id, { "x": x - step_x / 2, "y": y - step_y / 2 + offset_y, "shape": "none", "color": grid_color }),
			g.addNode('conn_ne_' + id, { "x": x + step_x / 2, "y": y - step_y / 2 + offset_y, "shape": "none", "color": grid_color }),
			g.addNode('conn_sw_' + id, { "x": x - step_x / 2, "y": y + step_y / 2 + offset_y, "shape": "none", "color": grid_color }),
			g.addNode('conn_se_' + id, { "x": x + step_x / 2, "y": y + step_y / 2 + offset_y, "shape": "none", "color": grid_color }),
		];
		g.addEdge(undefined, { "color": grid_color, "arrowHead": "none" }, conn[0], n);
		g.addEdge(undefined, { "color": grid_color, "arrowHead": "none" }, conn[1], n);
		g.addEdge(undefined, { "color": grid_color, "arrowHead": "none" }, n, conn[3]);
		g.addEdge(undefined, { "color": grid_color, "arrowHead": "none" }, n, conn[2]);
		// */

		row++;
		old_prefix = prefix;
		n++;
	}

	col++;
}

console.log('Done. %d shapes.', n);
