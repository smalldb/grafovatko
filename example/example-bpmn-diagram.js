// Input data
let graph = {
	nodes: [
		{
			id: "user",
			label: "User",
			x: 0,
			y: 0,
			graph: {
				nodes: [
					{ id: "start",       x:   0, y:   0, shape: "bpmn.event", event_type: "start" },
					{ id: "create",      x: 150, y:   0, shape: "bpmn.task", label: "Create request" },
					{ id: "split",       x: 300, y:   0, shape: "bpmn.gateway", gateway_type: "event" },
					{ id: "timeout",     x: 450, y:  75, shape: "bpmn.event", event_type: "intermediate", event_symbol: "timer" },
					{ id: "fail",        x: 600, y:  75, shape: "bpmn.task", label: "Timed out" },
					{ id: "fail_end",    x: 750, y:  75, shape: "bpmn.event", event_type: "end", event_symbol: "error" },
					{ id: "message",     x: 450, y: -75, shape: "bpmn.event", event_type: "intermediate", event_symbol: "message" },
					{ id: "success",     x: 600, y: -75, shape: "bpmn.task", label: "Success" },
					{ id: "success_end", x: 750, y: -75, shape: "bpmn.event", event_type: "end" },
				],
				edges: [
					{ start: "start", end: "create" },
					{ start: "create", end: "split" },
					{ start: "split", end: "timeout", points: [[300,  75]] },
					{ start: "split", end: "message", points: [[300, -75]] },
					{ start: "timeout", end: "fail" },
					{ start: "fail", end: "fail_end" },
					{ start: "message", end: "success" },
					{ start: "success", end: "success_end" },
				]
			}
		},
	],
	edges: [
	]
};

// Graph View
let gv = new G.GraphView('svg', graph);
//gv.graph.setLayout('dagre');

console.log('SVG Element:', gv.svgElement);
console.log('Graph:', gv.graph);

console.log('Done.');

