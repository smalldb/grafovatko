// Input data
var graph = {
	layout: 'dagre',
	layoutOptions: {
		rankdir: 'TB',
	},
	nodes: [
		{ id: 'r', color: "#d64", label: "Red", shape: "circle" },
		{ id: 'g', color: "#4c2", label: "Green" },
		{ id: 'b', color: "#66d", label: "Blue" },
	],
	edges: [
		{ start: 'r', end: 'g' },
		{ start: 'g', end: 'b' },
		{ start: 'b', end: 'g' },
		{ start: 'b', end: 'r', arrowHead: ['normal'] },
		{ start: 'b', end: 'r', arrowHead: ['vee'], arrowTail: ['vee'] },
		{ start: 'b', end: 'r', arrowHead: ['normal','vee'] },
		{ start: 'b', end: 'b' },
		{ start: 'g', end: 'g' },
	]
};

// Graph View
var gv = new G.GraphView('svg', graph);
console.log('Done.');

