// Build demo graph
let g = new G.Graph();
let r = 400;
let cx = 2 * r;
let cy = 2 * r;
let cn = g.addNode('s', { x: cx, y: cy, label: 'G.☺', shape: 'circle' });

let labels = [
	'zero',
	'one',
	'two',
	'three',
	'four',
	'five',
	'six',
	'seven',
	'eight',
	'nine',
	'ten',
	'eleven',
	'twelve',
];

// Demo graph: Inner circle
let h = 12;
for (let a = 0; a < 2 * Math.PI; a += Math.PI / 6) {
	let color = "hsl(" + (180. / Math.PI * a) + ",100%,40%)";
	let rn = g.addNode(undefined, { color: color, label: h, shape: 'circle', x: cx + 0.6 * r * Math.sin(a), y: cy - 0.6 * r * Math.cos(a) });
	let e = g.addEdge(undefined, { color: color, label: labels[h] }, cn, rn);
	h = (h + 1) % 12;
}

// Graph View
let gv = new G.GraphView('svg', g);

console.log('Done.');

