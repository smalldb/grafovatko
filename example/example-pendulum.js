// Build demo graph
let graph = new G.Graph();
let cx = 800;
let cy = 100;
let r = 350;

let g = 981; // cm/s^2
let max_angle = 0.8 * Math.PI / 2; // rad/2
let t0 = Date.now() / 1000; // s
let dt_prev = 0.1; // s
let t_prev = t0 - dt_prev; // s
let t_max = t0 + 60; // s

// Calculate pendulum position
// See:
//   - https://en.wikipedia.org/wiki/Pendulum_%28mathematics%29
//   - http://www.wolframalpha.com/input/?i=(++((T+-+t)%2F(T+-+n))+*+m+*+cos(sqrt(g+%2F+l)+*+t)+%2B+b+)%27
function getPendulumPosition(t, t_max, max_angle, angle0, g, l)
{
	let gl = Math.sqrt(g / l);
	let cos_tgl = Math.cos(t * gl);
	let sin_tgl = Math.sin(t * gl);
	let angle = ((t_max - t)/(t_max - t0)) * max_angle * cos_tgl + angle0;
	let vel = max_angle * (t_max * gl * sin_tgl - t * gl * sin_tgl + cos_tgl) / (t0 - t_max);

	return { angle: angle, vel: vel };
}

// Create a frame for pendulum
let rm = 1.2 * r;
let node_N     = graph.addNode('n', { color: '#666', x: cx - rm * 2/3, y: cy,      shape: 'rect' });
let node_vel   = graph.addNode('v', { color: '#666', x: cx - rm * 2/3, y: cy + rm, shape: 'rect' });
let node_angle = graph.addNode('a', { color: '#666', x: cx,            y: cy,      shape: 'rect' });
let node_fps   = graph.addNode('f', { color: '#666', x: cx + rm * 2/3, y: cy + rm, shape: 'rect' });
let node_time  = graph.addNode('t', { color: '#666', x: cx + rm * 2/3, y: cy,      shape: 'rect' });

// Fixed corners to avoid canvas resizing
let corner_tl  = graph.addNode('tl', { shape: 'point', color: '#666', x: cx - rm, y: cy });
let corner_tr  = graph.addNode('tr', { shape: 'point', color: '#666', x: cx + rm, y: cy });
let corner_bl  = graph.addNode('bl', { shape: 'point', color: '#666', x: cx - rm, y: cy + rm });
let corner_br  = graph.addNode('br', { shape: 'point', color: '#666', x: cx + rm, y: cy + rm });

let border_nodes = [
	corner_tl, node_N, node_angle, node_time, corner_tr,
	corner_br, node_fps, node_vel, corner_bl
];

for (let i = 0; i < border_nodes.length; i++) {
	let a = border_nodes[i];
	let b = border_nodes[(i + 1) % border_nodes.length];
	graph.addEdge('border_' + i, {arrowHead: [], color: '#666'}, a, b);
}

// Create the pendulum
let pendulum_count = 60;
let pendulum = [];
let pendulum_angle = [];
let last_pendulum = null;
let last_stick = null;
let angle0 = Math.PI / 2;
let p0 = getPendulumPosition(t0, t_max, max_angle, angle0, g, r);
for (let i = 0; i < pendulum_count; i++) {
	let c = 256 - Math.round(i * 64 / pendulum_count);
	let c_str = 'rgb(' + c + ',' + c + ',' + c + ')';
	last_pendulum = graph.addNode('pendulum' + i, { color: c_str, x: cx, y: cy + r, label: 'P', shape: 'circle', fill: '#fff' });
	last_stick = graph.addEdge('stick' + i, { color: c_str }, node_angle, last_pendulum);
	pendulum.push(last_pendulum);
	pendulum_angle.push(p0.angle);
}
last_pendulum.setAttr('color', '#a66');
last_pendulum.setAttr('fill', '#fff');
last_stick.setAttr('color', '#a66');

// Graph View
let gv = new G.GraphView('svg', graph);

// Animating function
function update() {
	// Get time since last frame
	let t = Date.now() / 1000;
	let dt = t - t_prev;
	let dt_filter = 0.1;
	let dt_avg = dt_filter * dt + (1 - dt_filter) * dt_prev;
	let use_avg_fps = (t - t0 > 5 / (1 - dt_filter));
	t_prev = t;
	dt_prev = dt_avg;


	// Get new pendulum position
	let p = getPendulumPosition(t, t_max, max_angle, angle0, g, r);
	let angle = p.angle;
	let vel = p.vel;

	// Stop at the end
	if (t > t_max) {
		vel = 0;
		angle = angle0;
		console.log('Animation finished.');
	}

	// Update graph
	pendulum_angle.shift();
	pendulum_angle.push(angle);
	for (let i = 0; i < pendulum_angle.length; i++) {
		pendulum[i].position.x = cx + r * Math.cos(pendulum_angle[i]);
		pendulum[i].position.y = cy + r * Math.sin(pendulum_angle[i]);
	}
	node_N.setAttr('label', "N = " + pendulum_count);
	node_vel.setAttr('label', "v = " + vel.toFixed(3) + " m/s");
	node_angle.setAttr('label', "angle = " + (90 - angle * 180 / Math.PI).toFixed(2) + "°");
	node_fps.setAttr('label', (1/(use_avg_fps ? dt_avg : dt)).toFixed(1) + " FPS");
	node_time.setAttr('label', "T - " + (t < t_max ? t_max - t : 0).toFixed(1) + " s");

	gv.update();

	// Enqueue next update
	if (t <= t_max) {
		setTimeout(update, 1);
	}
}

update();

console.log('Done.');

