// Build demo graph
let gv = new G.GraphView('svg');
let g = gv.graph;

let r = 240;
let cn = g.addNode('s', { x: 0, y: 0, label: 'G.☺', shape: 'circle' });

// Minute nodes
let m = 60;
let minute_nodes = [];
for (let a = 0; a < 2 * Math.PI; a += 2 * Math.PI / 60) {
	let color = "hsl(" + (180. / Math.PI * a) + ",85%,85%)";
	let rn = g.addNode(undefined, { color: color, fill: color, label: '  ', shape: 'circle', x: r * Math.sin(a), y: - r * Math.cos(a) });
	m = (m + 1) % 60;
	minute_nodes.push(rn);
}

// Hour nodes
let h = 12;
for (let a = 0; a < 2 * Math.PI; a += 2 * Math.PI / 12) {
	let color = "hsl(" + (180. / Math.PI * a) + ",100%,40%)";
	let rn = g.addNode(undefined, { color: '#fff', fill: color, label: h, shape: 'circle', x: r * Math.sin(a), y: - r * Math.cos(a) });
	h = (h + 1) % 12;
}

let hour_node = g.addNode('h', { color: '#000', shape: 'point', x: 0, y: 0 });
let hour_arrow = g.addEdge('hour', { color: '#000', arrowHead: '' }, cn, hour_node);
let min_arrow = g.addEdge('min', { color: '#000', arrowHead: 'diamond' }, cn, cn);
let sec_arrow = g.addEdge('sec', { color: '#888', arrowHead: '---odot' }, cn, cn);

// Graph View

// Animating function
function update() {
	let t = new Date();

	cn.setAttr('label', t.toLocaleTimeString());

	sec_arrow.end = minute_nodes[t.getSeconds()];
	min_arrow.end = minute_nodes[t.getMinutes()];

	let a = 2 * Math.PI * ((t.getHours() % 12) / 12 + 1/12 * t.getMinutes() / 60);
	hour_node.position.x = 0.6 * r * Math.sin(a);
	hour_node.position.y = - 0.6 * r * Math.cos(a);

	gv.update();

	// Enqueue next update
	setTimeout(update, Math.max(100, 1000 - (Date.now() % 1000)));
}

update();

console.log('Done.');

