// Hello world #1
{
	let graph_data = {
		nodes: [
			{ id: 'a', x:  50, y: 50, shape: 'circle' },
			{ id: 'b', x: 150, y: 50, shape: 'circle' }
		],
		edges: [
			{ id: 'e', start: 'a', end: 'b' }
		]
	};
	let gv = new G.GraphView('svg#svg1', graph_data);
}

// Hello world #2
{
	let gv = new G.GraphView('svg#svg2');
	let g = gv.graph;
	let a = g.addNode('a', { x:  50, y: 50, shape: 'circle' });
	let b = g.addNode('b', { x: 150, y: 50, shape: 'circle' });
	let e = g.addEdge('e', {}, a, 'b');   // Node may be specified by ID or GNode object.
}

// Hello world #3
{
	let graph_data = {
		nodes: [
			{ id: 'a', shape: 'circle' },
			{ id: 'b', shape: 'circle' }
		],
		edges: [
			{ id: 'e', start: 'a', end: 'b' }
		],
		layout: 'dagre',
		layoutOptions: {
			rankdir: 'LR'
		}
	};
	let gv = new G.GraphView('svg#svg3', graph_data);
}

// Hello world #4
{
	let gv = new G.GraphView('svg#svg4');
	let g = gv.graph;
	g.setLayout('dagre', { rankdir: 'LR' });
	let a = g.addNode('a', { shape: 'circle' });
	let b = g.addNode('b', { shape: 'circle' });
	let e = g.addEdge('e', {}, a, 'b');   // Node may be specified by ID or GNode object.
}

console.log('Done.');

