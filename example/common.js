/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// Version info
console.log('Grafovatko %s.', G.version);
document.getElementById('version').textContent = 'Grafovatko ' + G.version;

// Measure "render" time
let renderStart = new Date().getTime();
window.onload = function() { 
	window.setTimeout(function() {
		let elapsed = new Date().getTime()-renderStart;
		console.log('Rendered in %f ms.', elapsed); 
		document.getElementById('timer').textContent = 'Loaded & rendered in ' + (elapsed - 1) + ' ms.';
	}, 1);
} 

