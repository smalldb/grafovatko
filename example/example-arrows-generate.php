#!/usr/bin/env php
<?php

if (php_sapi_name() !== 'cli') {
	die('CLI only.');
}

$styles = [ 'box', 'crow', 'curve', 'icurve', 'diamond', 'dot', 'inv', 'none', 'normal', 'tee', 'vee' ];
$sides = [ '', 'l', 'r' ];
$fills = [ '', 'o' ];

$nodes = [];
$edges = [];

$x = 0;
$y = 0;
$x_step = 150;
$y_step = 50;

foreach ($styles as $style) {
	$x = 0;
	$y += $y_step;
	$prev_node = 'start_'.$style;
	$prev_head = 'none';
	$nodes[] = [ 'id' => $prev_node, 'shape' => 'point', 'x' => $x + $x_step * 1/3, 'y' => $y ];
	$gv[] = $prev_node."[shape=point];";
	foreach ($fills as $fill) {
		foreach ($sides as $side) {
			$x += $x_step;
			$head = $fill.$side.$style;
			$nodes[] = [ 'id' => $head, 'label' => $head, 'color' => '#aaa', 'x' => $x, 'y' => $y ];
			$edges[] = [ 'start' => $prev_node, 'end' => $head, 'arrowHead' => $head, 'arrowTail' => $prev_head ];
			$gv[] = $head.';';
			$gv[] = $prev_node.'->'.$head.'[arrowhead='.$head.',arrowtail='.$prev_head.'];';
			$prev_node = $head;
			$prev_head = $head;
		}
	}
	$x += $x_step * 2/3;
	$nodes[] = [ 'id' => 'end_'.$style, 'shape' => 'point', 'x' => $x, 'y' => $y ];
	$edges[] = [ 'start' => $prev_node, 'end' => 'end_'.$style, 'arrowHead' => 'none', 'arrowTail' => $prev_head ];
	$gv[] = 'end_'.$style.'[shape=point];';
	$gv[] = $prev_node.'->'.'end_'.$style.'[arrowhead=none,arrowtail='.$prev_head.'];';
}

ob_start();

echo "// Input data (generated)\n";
echo "let graph = ", json_encode([
	'nodes' => $nodes,
	'edges' => $edges,
	//'layout' => 'dagre',
	//'layoutOptions' => [
	//	'rankdir' => 'LR',
	//	'nodesep' => '20',
	//],
]), ";\n";

echo <<<eof

// Graph View
let gv = new G.GraphView('svg', graph);

console.log('Done.');

eof;

file_put_contents(__DIR__.'/example-arrows.js', ob_get_clean());

ob_start();
echo "digraph G {\n";
echo "\trankdir=LR;\n";
echo "\tnodesep=0.2;\n";
echo "\tedge[dir=both];\n";
echo "\tnode[fontcolor=\"#aaaaaa\", fontname=\"sans-serif\", fontsize=11, color=\"#aaaaaa\", shape=box];\n";
foreach (array_reverse($gv) as $gv_line) {
	echo "\t", $gv_line, "\n";
}
echo "}\n";

$dot_file = __DIR__.'/example-arrows.dot';
file_put_contents($dot_file, ob_get_clean());
$f = escapeshellarg($dot_file);
exec("dot $f -Tsvg -o $f.svg");

