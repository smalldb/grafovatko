// Input data
var graph = {
	nodes: [
		{
			id: "rgb",
			color: "#888",
			label: "RGB",
			x: 100,
			y: 100,
			graph: {
				nodes: [
					{ id: "r", color: "#d64", x:  50, y:  50, label: "Red", shape: "circle" },
					{ id: "g", color: "#4c2", x: 150, y: 150, label: "Green", shape: "circle" },
					{ id: "b", color: "#66d", x:  50, y: 300, label: "Blue", shape: "circle" },
				],
				edges: [
					{ start: "r", end: "g", color: "#d64", points: [[150, 50]], label: "R→G" },
					{ start: "g", end: "b", color: "#4c2", points: [[150, 300]], label: "G→B" },
					{ start: "b", end: "r", color: "#66d", label: "B→R" },
				]
			}
		}, {
			id: "cmyk",
			color: "#888",
			label: "CMYK",
			x: 550,
			y: 100,
			graph: {
				nodes: [
					{ id: "c", color: "#4aa", x: 150, y:  50, label: "Cyan" },
					{ id: "k", color: "#000", x: 150, y: 150, label: "Black" },
					{
						id: "my", color: "#aaa", x: 150, y: 250, label: "M+Y",
						graph: {
							nodes: [
								{ id: "m", color: "#d4d", x:  50, y: 50, label: "Magenta", shape: "ellipse" },
								{ id: "y", color: "#cc0", x: 250, y: 50, label: "Yellow", shape: "ellipse" },
							]
						}
					}
				],
				edges: [
					{ start: "c", end: "k", color: "#4aa", label: "C→K" },
					{ start: "m", end: "k", color: "#d4d", points: [[50, 150]], label: "M→K" },
					{ start: "y", end: "k", color: "#cc0", points: [[250, 150]], label: "Y→K" },
				]
			}
		},
	],
	edges: [
		{ start: "rgb", end: "cmyk", color: "#888", label: "RGB→CMYK" },
		{ start: "g", end: "c", color: "#4c2", label: "G→C" },
		{ start: "b", end: "my", color: "#66d", points: [[100,300], [125, 300], [400, 300], [500,300], [550,300]], label: "B→MY" }
	]
};

// Graph View
var gv = new G.GraphView('svg', graph);
//gv.graph.setLayout('dagre');

console.log('SVG Element:', gv.svgElement);
console.log('Graph:', gv.graph);

console.log('Done.');

