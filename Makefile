lib_src=class/*.ts
vendor_libs=lib/require.js
version=$(shell git describe --always)

default: lib
all: lib dist website

lib: build/grafovatko.js
dist: dist/grafovatko.min.js dist/grafovatko.zip dist/grafovatko.tar.gz
website: build/website
cli: bin/grafovatko

clean: clean-build clean-deps

clean-build:
	[ ! -d build ] || rm -rf -- build
	[ ! -d dist ] || rm -rf -- dist
	[ ! -d .rpt2_cache ] || rm -rf -- .rpt2_cache

clean-deps:
	[ ! -d node_modules ] || rm -rf -- node_modules

.PHONY: default all dist website cli clean clean-build clean-deps


build:
	mkdir build

node_modules:
	npm install


dist/grafovatko.min.js: node_modules build build/grafovatko.js README.md LICENSE package.json
	[ ! -d dist ] || rm -rf -- dist
	mkdir dist
	cp -- README.md LICENSE dist/
	npm run uglify

dist/grafovatko.zip: dist/grafovatko.min.js
	[ ! -e $@ ] || rm -f -- $@
	mkdir dist/grafovatko-$(version)
	cp dist/README.md dist/LICENSE dist/grafovatko.min.js dist/grafovatko.min.js.map dist/grafovatko-$(version)
	cd dist && zip -q grafovatko.zip -r grafovatko-$(version)
	rm -fr -- dist/grafovatko-$(version)

dist/grafovatko.tar.gz: dist/grafovatko.min.js
	[ ! -e $@ ] || rm -f -- $@
	mkdir dist/grafovatko-$(version)
	cp dist/README.md dist/LICENSE dist/grafovatko.min.js dist/grafovatko.min.js.map dist/grafovatko-$(version)
	cd dist/ && tar -czf grafovatko.tar.gz grafovatko-$(version)
	rm -fr -- dist/grafovatko-$(version)

build/grafovatko.js: node_modules build $(lib_src) Makefile package.json rollup.config.js tsconfig.json build/version.json build/Grafovatko.index.js
	npm run rollup

build/version.json: Makefile
	echo '{ "version": "$(version)" }' > build/version.json

build/Grafovatko.index.js: node_modules build $(lib_src) Makefile package.json
	( cd build/ \
		&& find ../class -type f -name '*.ts' -printf 'export { default as %f } from "%p";\n' \
		| sed -e 's/ as G/ as /' -e 's/\.ts//g' \
		&& echo 'var version = "$(version)"; export {version};' \
		) > build/Grafovatko.index.js

class/GDiaShapeLibrary.ts: class/GDiaShapeLibrary.ts.template bin/dia_shape_import
	bin/dia_shape_import


bin/grafovatko: build/grafovatko.js cli.js
	( echo '#!/usr/bin/env node' ; cat $(vendor_libs) $^ ; echo ; echo 'require("cli");' ) > $@
	chmod +x $@

build/website: build/grafovatko.js README.md example/* dist
	[ -d build/website ] || mkdir build/website
	tail -n +3 README.md | pandoc --standalone -M title=Grafovatko -M css=example/style.css -f markdown -fmarkdown-implicit_figures -o build/website/index.html
	cp -rL LICENSE example/ build/website/
	cp -rL dist/ build/website/
	[ -d build/website/build ] || mkdir build/website/build
	cp build/grafovatko.* build/website/build/

build/cli.js: node_modules build cli.ts $(lib_src) Makefile package.json rollup.config.js tsconfig.json build/version.json build/Grafovatko.index.js
	npm run rollup-cli

