/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GShapeCircle from './GShapeCircle';
import GNode from './GNode';
import GSvg from './GSvg';
import GSvgPath from './GSvgPath';
import GSize from "./GSize";


export default class GShapeBpmnEvent extends GShapeCircle
{
	public static readonly eventTypes = [
		'start', 'intermediate', 'end'
	];

	public static readonly eventSymbols = [
		'none', 'message', 'timer', 'error', 'escalation',
		'cancel', 'compensation', 'conditional', 'link',
		'signal', 'terminate', 'multiple', 'parallel_multiple'
	];

	
	public renderElements(svg: GSvg, node: GNode): any[]
	{
		let eventType: string = node.attr('event_type', 'starting');
		let eventSymbol: string = node.attr('event_symbol', 'none');
		let isNoninterrupting: boolean = node.attr('event_is_interrupting', false);
		let isThrowing: boolean = eventType == 'end' ? true : node.attr('event_is_throwing', false);

		let r = node.getSize().width / 2;
		let rs = r / 40; // Symbols are 40x40 px.
		let border;
		switch (eventType) {
			default:
			case 'start':
				border = this.renderStartBorder(svg, node, r, isNoninterrupting);
				break;
			case 'intermediate':
				border = this.renderIntermediateBorder(svg, node, r, isNoninterrupting);
				break;
			case 'end':
				border = this.renderEndBorder(svg, node, r);
				break;
		}

		let sym;
		switch (eventSymbol) {
			default:
			case 'none':
				break;
			case 'message':
				sym = this.renderMessageSymbol(svg, node, rs, isThrowing);
				break;
			case 'timer':
				sym = this.renderTimerSymbol(svg, node, rs);
				break;
			case 'error':
				sym = this.renderErrorSymbol(svg, node, rs, isThrowing);
				break;
			case 'escalation':
				sym = this.renderEscalationSymbol(svg, node, rs, isThrowing);
				break;
			case 'cancel':
				sym = this.renderCancelSymbol(svg, node, rs, isThrowing);
				break;
			case 'compensation':
				sym = this.renderCompensationSymbol(svg, node, rs, isThrowing);
				break;
			case 'conditional':
				sym = this.renderConditionalSymbol(svg, node, rs, isThrowing);
				break;
			case 'link':
				sym = this.renderLinkSymbol(svg, node, rs, isThrowing);
				break;
			case 'signal':
				sym = this.renderSignalSymbol(svg, node, rs, isThrowing);
				break;
			case 'terminate':
				sym = this.renderTerminateSymbol(svg, node, rs);
				break;
			case 'multiple':
				sym = this.renderMultipleSymbol(svg, node, rs, isThrowing);
				break;
			case 'parallel_multiple':
				sym = this.renderParallelMultipleSymbol(svg, node, rs, isThrowing);
				break;
		}

		return [ ...border, ...sym ];
	}


	public getMinSize(node: GNode): GSize
	{
		return new GSize(3 * this.get1em(node) + 3 * this.strokeWidth);
	}


	protected renderStartBorder(svg: GSvg, node: GNode, r: number, isNoninterrupting: boolean): any[]
	{
		return [
			svg.circle({
				'cx': 0,
				'cy': 0,
				'r': r,
				'stroke-width': this.strokeWidth,
				'stroke': node.attr('color'),
				'stroke-dasharray': isNoninterrupting ? 5 * this.strokeWidth : undefined,
				'fill': node.attr('fill'),
			})
		];
	}

	protected renderIntermediateBorder(svg: GSvg, node: GNode, r: number, isNoninterrupting: boolean): any[]
	{
		return [
			svg.circle({
				'cx': 0,
				'cy': 0,
				'r': r,
				'stroke-width': 0.75 * this.strokeWidth,
				'stroke': node.attr('color'),
				'stroke-dasharray': isNoninterrupting ? 5 * this.strokeWidth : undefined,
				'fill': node.attr('fill'),
			}),
			svg.circle({
				'cx': 0,
				'cy': 0,
				'r': r - 2 * this.strokeWidth,
				'stroke-width': 0.75 * this.strokeWidth,
				'stroke': node.attr('color'),
				'stroke-dasharray': isNoninterrupting ? 4.333 * this.strokeWidth : undefined,
				'fill': node.attr('fill'),
			})
		];
	}

	protected renderEndBorder(svg: GSvg, node: GNode, r: number): any[]
	{
		return [
			svg.circle({
				'cx': 0,
				'cy': 0,
				'r': r - this.strokeWidth,
				'stroke-width': 2 * this.strokeWidth,
				'stroke': node.attr('color'),
				'fill': node.attr('fill'),
			})
		];
	}


	protected renderMessageSymbol(svg: GSvg, node: GNode, s: number, isThrowing: boolean): any[]
	{
		if (isThrowing) {
			return [
				svg.path({
					"d": new GSvgPath().scale(s)
						.M(-14, -12)
						.L( 14, -12)
						.L(  0,  -3)
						.Z()
						.M(-16,  -8)
						.L(  0,   2)
						.L( 16,  -8)
						.L( 16,  12)
						.L(-16,  12)
						.Z(), 
					"fill": node.attr('color'),
					"stroke-width": 0.5 * this.strokeWidth,
					"stroke": node.attr('color'),
				}),
			];
		} else {
			return [
				svg.path({
					"d": new GSvgPath().scale(s)
						.M(-16, -12)
						.L( 16, -12)
						.L( 16,  12)
						.L(-16,  12)
						.Z()
						.L(  0,   0)
						.L( 16, -12),
					"fill": "none",
					"stroke-width": 0.5 * this.strokeWidth,
					"stroke": node.attr('color'),
				}),
			];
		}
	}


	protected renderTimerSymbol(svg: GSvg, node: GNode, s: number): any[]
	{
		return [
			svg.circle({
				"cx": 0,
				"cy": 0,
				"r": 20 * s,
				"fill": "none",
				"stroke-width": 0.75 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
			svg.path({
				"d": new GSvgPath().scale(s)
					.M(  0.00, -16.00).L(  0.00, -12.00)
					.M(  0.00,  12.00).L(  0.00,  16.00)
					.M(-16.00,   0.00).L(-12.00,   0.00)
					.M( 12.00,   0.00).L( 16.00,   0.00)
					.M( -6.00,  10.00).L( -8.30,  13.70)
					.M(-10.00,   6.00).L(-13.70,   8.30)
					.M(-10.00,  -6.00).L(-13.65,  -8.35)
					.M( -6.00, -10.00).L( -8.25, -13.65)
					.M(  6.00, -10.00).L(  8.27, -13.75)
					.M( 10.00,  -6.00).L( 13.77,  -8.25)
					.M( 10.00,   6.00).L( 13.77,   8.29)
					.M(  6.00,  10.00).L(  8.27,  13.79)
					.M(  0.00,   0.00).L(  8.00,   0.00)
					.M(  0.00,   0.00).L(  2.47, -12.53),
				"fill": "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderErrorSymbol(svg: GSvg, node: GNode, s: number, isThrowing: boolean): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
					.M(-18,  12)
					.L( -6, -16)
					.L(  6,   4)
					.L( 18, -12)
					.L(  6,  16)
					.L( -6,  -4)
					.Z(), 
				"fill": isThrowing ? node.attr('color') : "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderEscalationSymbol(svg: GSvg, node: GNode, s: number, isThrowing: boolean): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
					.M(-14,  18)
					.L(  0, -18)
					.L( 14,  18)
					.L(  0,   4)
					.Z(), 
				"fill": isThrowing ? node.attr('color') : "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderCancelSymbol(svg: GSvg, node: GNode, s: number, isThrowing: boolean): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
					.M( -16, -12)
					.L(  -4,   0)
					.L( -16,  12)
					.L( -12,  16)
					.L(   0,   4)
					.L(  12,  16)
					.L(  16,  12)
					.L(   4,   0)
					.L(  16, -12)
					.L(  12, -16)
					.L(   0,  -4)
					.L( -12, -16)
					.Z(), 
				"fill": isThrowing ? node.attr('color') : "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderCompensationSymbol(svg: GSvg, node: GNode, s: number, isThrowing: boolean): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
                    			.M(-4, -16).L(-4, 16).L(-20, 0).Z()
                    			.M(12, -16).L(12, 16).L( -4, 0).Z(), 
				"fill": isThrowing ? node.attr('color') : "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderConditionalSymbol(svg: GSvg, node: GNode, s: number, isThrowing: boolean): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
					.M(-18, -20).L(-18,  20)
					.L( 18,  20).L( 18, -20).Z()
                    			.M(-14, -12).L( 14, -12)
                    			.M(-14,  -4).L( 14,  -4)
                    			.M(-14,   4).L( 14,   4)
                    			.M(-14,  12).L( 14,  12),
				"fill": isThrowing ? node.attr('color') : "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderLinkSymbol(svg: GSvg, node: GNode, s: number, isThrowing: boolean): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
					.M(-20,  -8)
					.L(  8,  -8)
					.L(  8, -16)
					.L( 20,   0)
					.L(  8,  16)
					.L(  8,   8)
					.L(-20,   8)
					.Z(),
				"fill": isThrowing ? node.attr('color') : "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderSignalSymbol(svg: GSvg, node: GNode, s: number, isThrowing: boolean): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
					.M(  0, -20)
					.L(-18,  12)
					.L( 18,  12)
					.Z(),
				"fill": isThrowing ? node.attr('color') : "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderTerminateSymbol(svg: GSvg, node: GNode, s: number): any[]
	{
		return [
			svg.circle({
				"cx": 0,
				"cy": 0,
				"r": 20 * s,
				"fill": node.attr('color'),
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderMultipleSymbol(svg: GSvg, node: GNode, s: number, isThrowing: boolean): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
					.M(  0.0, -20.0)
					.L( 19.0,  -6.2)
					.L( 11.8,  16.2)
					.L(-11.8,  16.2)
					.L(-19.0,  -6.2)
					.Z(),
				"fill": isThrowing ? node.attr('color') : "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderParallelMultipleSymbol(svg: GSvg, node: GNode, s: number, isThrowing: boolean): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
					.M(  3, -20)
					.L(  3,  -3)
					.L( 20,  -3)
					.L( 20,   3)
					.L(  3,   3)
					.L(  3,  20)
					.L( -3,  20)
					.L( -3,   3)
					.L(-20,   3)
					.L(-20,  -3)
					.L( -3,  -3)
					.L( -3, -20)
					.Z(),
				"fill": isThrowing ? node.attr('color') : "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}

}

