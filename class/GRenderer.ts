/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GBoundingBox from "./GBoundingBox";
import GEdge from './GEdge';
import GGraph from './GGraph';
import GNode from './GNode';
import GPoint from './GPoint';
import GSvg from './GSvg';
import GSvgPath from './GSvgPath';
import GTransformation from './GTransformation';
import GVector from './GVector';


export default class GRenderer
{

	public renderGraph(svg: GSvg, graph: GGraph, transformation?: GTransformation): any
	{
		let elements;

		// Render custom SVG elements
		if (graph.extraSvg) {
			elements = this.renderSvgJson(svg, graph.extraSvg);
		} else {
			elements = [];
		}

		// Render nodes
		graph.forEachNode((node: GNode) => {
			elements.push(this.renderNode(svg, node));
		});

		// Render edges
		graph.forEachEdge((edge: GEdge) => {
			elements.push(this.renderEdge(svg, edge));
		});

		// Debug: Render bounding boexes
		/*
		graph.forEachEdge((e: GEdge) => {
			elements.push(this.renderBoundingBox(svg, e.getBoundingBox().toRef(graph), "#0f0"));
		});
		if (graph === graph.rootGraph) {
			elements.push(this.renderBoundingBox(svg, graph.getBoundingBox(), "#f08"));
			graph.forEachNestedGraph((g: GGraph, node: GNode, depth: number) => {
				let tr = g.getAbsTransformation();
				elements.push(svg.circle({
					cx: tr.x,
					cy: tr.y,
					r: 3,
					fill: '#faa',
				}));
				elements.push(this.renderBoundingBox(svg, g.getBoundingBox().toRef(graph), "#f00"));
			});
		}
		// */

		// Render container with the prepared elements
		return svg.g({
			'class': 'graph',
			'transform': transformation ? transformation.toCss() : graph.getBoundingBox().getCenteringTransformation().toCss(),
		}, elements);
	}


	public renderNode(svg: GSvg, node: GNode): any
	{
		return node.getShape().render(svg, node, (graph: GGraph, transformation?: GTransformation) => this.renderGraph(svg, graph, transformation));
	}


	public renderEdge(svg: GSvg, edge: GEdge): any
	{
		if (edge.attr('hidden')) {
			return [];
		}

		let strokeWidth = 1.5;

		let line: any = {
			'fill': 'none',
			'stroke': edge.attr('color'),
			'stroke-width': strokeWidth,
			'stroke-dasharray': edge.attr('stroke_dasharray'),	// FIXME: Replace with generic styles
		};
		let tooltip = edge.attr('tooltip');
		let dbgPath = new GSvgPath();
		let debug: any = {
			'd': dbgPath,
			'stroke': 'rgba(0,0,0,0.3)',
		};
		let arrowHeadF = {};
		let arrowHeadO = {};
		let arrowTailF = {};
		let arrowTailO = {};

		// Convert points to edge's ref (just to be sure)
		let points = edge.points.map((p) => p.toRef(edge.graph));

		// Start & end positions
		let startPos = edge.start.getPosition().toRef(edge.graph);
		let endPos = edge.end.getPosition().toRef(edge.graph);

		// First/last waypoint (mid-point of the arrow)
		let firstPoint = points.length > 0 ? points[0] : endPos;
		let lastPoint = points.length > 0 ? points[points.length - 1] : startPos;

		// Arrow vectors
		let vTail = edge.start.getConnectorVector(firstPoint).toRef(edge.graph);
		let vHead = edge.end.getConnectorVector(lastPoint).toRef(edge.graph);

		// Update arrow head/tail, calculate their length
		let lTail = this.renderArrowHead(arrowTailF, arrowTailO, vTail, strokeWidth, edge.attr('color'), edge.getArrowTail());
		let lHead = this.renderArrowHead(arrowHeadF, arrowHeadO, vHead, strokeWidth, edge.attr('color'), edge.getArrowHead());

		// Debug: Show cross-graph edges without vectors and points
		/*
		if (edge.start.getPosition().ref !== edge.end.getPosition().ref) {
			dbgPath.Mp(startPos).Lp(endPos);
		}
		// */
		// Debug: Dump cross-graph edges
		/*
		if (edge.start.getPosition().ref !== edge.end.getPosition().ref) {
			console.log("Cross-graph edge:", edge.start.id, "→", edge.end.id, edge);
			console.log("\tref:", edge.graph.getAbsTransformation().toString());
			console.log("\tS:", edge.start.getPosition().toString());
			console.log("\tE:", edge.end.getPosition().toString());
			console.log("\tS':", startPos.toString());
			console.log("\tE':", endPos.toString());
			console.log("\tVs:", v_tail.toString());
			console.log("\tVe:", v_head.toString());
		}
		// */

		// Debug: Show connector vectors
		/*
		dbgPath.Mp(v_tail.start).Lp(v_tail.end);
		dbgPath.Mp(v_head.start).Lp(v_head.end);
		// */

		// Debug: Render line's points
		/*
		for (let i = 0; i < points.length; i++) {
			dbgPath.Mp(points[i]).m(-3, -3).l(6, 6).m(0, -6).l(-6, 6);
		}
		// */

		// Render line path
		if (points.length == 0) {
			// No waypoints
			line.d = new GSvgPath(vTail.getPointFromEnd(lTail))
				.Lp(vHead.getPointFromEnd(lHead))
				.toString();
		} else {
			// With waypoints
			if (points.length % 2 == 1) {
				// Quadratic bezier line requires even number of points, ...
				line.d = new GSvgPath(vTail.getPointFromEnd(lTail))
					._v('Q', points)
					._p(vHead.getPointFromEnd(lHead))
					.toString();
			} else {
				// ... but one point is missing, se we replace it with a simple line
				line.d = new GSvgPath(vTail.getPointFromEnd(lTail))
					._v('Q', points)
					.Lp(vHead.getPointFromEnd(lHead))
					.toString();
			}
		}

		// Put everything together
		return svg.g({
			'class': 'edge',
			'key': edge.id,
			'opacity': edge.attr('opacity'),
		}, [
			svg.path(line),
			svg.path(arrowHeadF),
			svg.path(arrowHeadO),
			svg.path(arrowTailF),
			svg.path(arrowTailO),
			this.renderEdgeLabel(svg, edge),
			tooltip != "" ? svg.title([], [tooltip]) : null,
			svg.path(debug),
		]);
	}


	protected renderArrowHead(arrowFill: any, arrowOutline: any, v: GVector, strokeWidth: number, color: string, dotArrowHead: string): number
	{
		let scale = 1 + strokeWidth / 2;
		let w = 2 * scale;
		let l = 6 * scale;
		let h = l / 2;

		let offset = 0;
		let pathO = new GSvgPath();
		let pathF = new GSvgPath();

		let re = /(o?)([lr]?)(box|crow|curve|icurve|diamond|dot|empty|inv|none|normal|tee|vee|-)/g;
		let match;

		while ((match = re.exec(dotArrowHead)) !== null) {
			let outline = match[1] == 'o';
			let hasLeftSide = match[2] != 'r';
			let hasRightSide = match[2] != 'l';
			let arrowHead = match[3];

			if (arrowHead == 'empty') {
				outline = true;
				arrowHead = 'normal';
			}

			let path = outline ? pathO : pathF;

			switch (arrowHead) {
				default:
					offset += l;
					break;

				case 'none':
					break;

				case 'box':
					if (hasLeftSide) {
						path.M(offset, h);
						path.L(offset + l, h);
					} else {
						path.M(offset, 0);
						path.L(offset + l, 0);
					}
					if (hasRightSide) {
						path.L(offset + l, -h);
						path.L(offset, -h);
					} else {
						path.L(offset + l, 0);
						path.L(offset, 0);
					}
					path.z();
					offset += l;
					break;

				case 'crow':
					path.M(offset + l, 0);
					if (hasLeftSide) {
						path.L(offset, w);
						path.L(offset + 0.5 * l, 0);
					}
					path.L(offset, 0);
					if (hasRightSide) {
						path.L(offset + 0.5 * l, 0);
						path.L(offset, -w);
					}
					path.z();
					offset += l;
					break;

				case 'curve':
					if (hasLeftSide && hasRightSide) {
						pathO.M(offset + h, h);
						pathO.A(h, h, 0, false, true, offset + h, -h);
					} else if (hasLeftSide) {
						pathO.M(offset, 0);
						pathO.A(h, h, 0, false, false, offset + h, h);
					} else if (hasRightSide) {
						pathO.M(offset, 0);
						pathO.A(h, h, 0, false, true, offset + h, -h);
					}
					pathO.M(offset, 0);
					pathO.L(offset + l, 0);
					offset += l;
					break;

				case 'icurve':
					if (hasLeftSide && hasRightSide) {
						pathO.M(offset + h, h);
						pathO.A(h, h, 0, false, false, offset + h, -h);
					} else if (hasLeftSide) {
						pathO.M(offset + 2 * h, 0);
						pathO.A(h, h, 0, false, true, offset + h, h);
					} else if (hasRightSide) {
						pathO.M(offset + 2 * h, 0);
						pathO.A(h, h, 0, false, false, offset + h, -h);
					}
					pathO.M(offset, 0);
					pathO.L(offset + l, 0);
					offset += l;
					break;

				case 'diamond':
					path.M(offset, 0);
					if (hasLeftSide) {
						path.L(offset + l / 2, w);
					}
					path.L(offset + l, 0);
					if (hasRightSide) {
						path.L(offset + l / 2, -w);
					}
					path.z();
					offset += l;
					break;

				case 'dot':
					path.M(offset, 0);
					if (hasLeftSide) {
						path.A(h, h, 0, false, false, offset + l, 0);
					} else {
						path.L(offset + l, 0);
					}
					if (hasRightSide) {
						path.A(h, h, 0, false, false, offset, 0);
					}
					path.z();
					offset += l;
					break;

				case 'inv':
					path.M(offset + l, 0);
					if (hasLeftSide) {
						path.L(offset, w);
					} else {
						path.L(offset, 0);
					}
					if (hasRightSide) {
						path.L(offset, -w);
					} else {
						path.L(offset, 0);
					}
					path.z();
					offset += l;
					break;

				case 'normal':
					path.M(offset, 0);
					if (hasLeftSide) {
						path.L(offset + l, w);
					} else {
						path.L(offset + l, 0);
					}
					if (hasRightSide) {
						path.L(offset + l, -w);
					} else {
						path.L(offset + l, 0);
					}
					path.z();
					offset += l;
					break;

				case 'tee':
					path.M(offset, 0);
					path.L(offset + l / 4, 0);

					path.M(offset + l / 2, 0);
					path.L(offset + l, 0);

					if (hasLeftSide) {
						path.M(offset + l / 4, h);
						path.L(offset + l / 2, h);
					} else {
						path.M(offset + l / 4, 0);
						path.L(offset + l / 2, 0);
					}
					if (hasRightSide) {
						path.L(offset + l / 2, -h);
						path.L(offset + l / 4, -h);
					} else {
						path.L(offset + l / 2, 0);
						path.L(offset + l / 4, 0);
					}
					path.z();
					offset += l;
					break;

				case 'vee':
					path.M(offset, 0);
					if (hasLeftSide) {
						path.L(offset + l, w);
						path.L(offset + 0.5 * l, 0);
					}
					path.L(offset + l, 0);
					if (hasRightSide) {
						path.L(offset + 0.5 * l, 0);
						path.L(offset + l, -w);
					}
					path.z();
					offset += l;
					break;

				case '-':
					path.M(offset, 0);
					path.L(offset + l, 0);
					offset += l;
					break;
			}
		}

		arrowOutline.d = pathO;
		arrowOutline['stroke-width'] = strokeWidth;
		arrowOutline.stroke = color;
		arrowOutline.fill = 'none';
		arrowOutline.transform = 'translate(' + v.end.toCss() + ')'
			+ ' rotate(' + (180 + v.angleDeg()) + ')';

		arrowFill.d = pathF;
		arrowFill['stroke-width'] = strokeWidth;
		arrowFill.stroke = color;
		arrowFill.fill = color;
		arrowFill.transform = 'translate(' + v.end.toCss() + ')'
			+ ' rotate(' + (180 + v.angleDeg()) + ')';

		return offset;
	}


	/**
	 * Render edge's label.
	 *
	 * @param svg Hyperscript-compatible element factory/renderer.
	 * @param edge Edge of the label to render.
	 */
	protected renderEdgeLabel(svg: GSvg, edge: GEdge): any
	{
		let label = edge.attr('label');

		// Don't render no label
		if (label === undefined || label === null) {
			return null;
		}

		// Get label position
		let p;
		if (edge.labelPosition) {
			// Explicit position
			p = edge.labelPosition.toRef(edge.graph);
		} else if (edge.points.length > 0) {
			// Middle point if points are specified
			p = edge.points[Math.floor(edge.points.length / 2)].toRef(edge.graph);
		} else {
			// Middle of the line if there are no points
			let s = edge.start.getPosition().toRef(edge.graph);
			let e = edge.end.getPosition().toRef(edge.graph);
			p = new GPoint((s.x + e.x) / 2, (s.y + e.y) / 2, edge.graph);
		}

		// Render the label
		return svg.text({
			'text-anchor': "left",
			'font-size': "11pt",
			'font-family': "sans-serif",
			'fill': edge.attr('color'),
			'x': p.x,
			'y': p.y,
		}, [label]);
	}


	protected renderBoundingBox(svg: GSvg, bb: GBoundingBox, color: string = "#f00"): any
	{
		return svg.rect({
			"x": bb.tlx,
			"y": bb.tly,
			"width": bb.getWidth(),
			"height": bb.getHeight(),
			"stroke": color,
			"stroke-width": 1,
			"stroke-dasharray": 5,
			"opacity": 0.66,
			"fill": "transparent",
		});
	}


	protected renderSvgJson(svg: GSvg, svgJson: any[]): any
	{
		return svgJson.map((e) => svg.el(e[0], e[1] || {}, e[2] ? this.renderSvgJson(svg, e[2]) : []));
	}
}

