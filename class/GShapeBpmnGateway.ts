/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GNode from './GNode';
import GShapeDiamond from './GShapeDiamond';
import GSize from "./GSize";
import GSvg from './GSvg';
import GSvgPath from './GSvgPath';


export default class GShapeBpmnGateway extends GShapeDiamond
{
	
	public renderElements(svg: GSvg, node: GNode): any[]
	{
		return [
			this.renderOutlineElement(svg, node),
			this.renderSymbol(svg, node),
		];
	}


	public getMinSize(node: GNode): GSize
	{
		return new GSize(4 * this.get1em(node) + this.strokeWidth);
	}


	protected renderSymbol(svg: GSvg, node: GNode): any
	{
		let gatewayType: string = node.attr('gateway_type', 'none');
		let instantiate: boolean = node.attr('gateway_instantiate', false);
		let s = 2 * this.get1em(node) / 40; // Scale symbols from 40px to 2em

		switch (gatewayType) {
			default:
			case 'none':
			case 'plain':
				return null;
			case 'exclusive':
				return this.renderExclusiveSymbol(svg, node, s);
			case 'event':
				return this.renderEventSymbol(svg, node, s, instantiate);
			case 'parallel_event':
				return this.renderParallelEventSymbol(svg, node, s);
			case 'inclusive':
				return this.renderInclusiveSymbol(svg, node, s);
			case 'complex':
				return this.renderComplexSymbol(svg, node, s);
			case 'parallel':
				return this.renderParallelSymbol(svg, node, s);
		}
	}


	protected renderExclusiveSymbol(svg: GSvg, node: GNode, s: number): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
					.M(-14, -18)
					.L( -8, -18)
					.L(  0,  -5)
					.L(  8, -18)
					.L( 14, -18)
					.L(  3,   0)
					.L( 14,  18)
					.L(  8,  18)
					.L(  0,   5)
					.L( -8,  18)
					.L(-14,  18)
					.L( -3,   0)
					.Z(), 
				"fill": node.attr('color'),
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderInclusiveSymbol(svg: GSvg, node: GNode, s: number): any[]
	{
		return [
			svg.circle({
				'cx': 0,
				'cy': 0,
				'r': 20 * s - this.strokeWidth,
				'stroke-width': 2 * this.strokeWidth,
				'stroke': node.attr('color'),
				'fill': 'transparent',
			}),
		];
	}


	protected renderEventSymbol(svg: GSvg, node: GNode, s: number, instantiate: boolean): any[]
	{
		return [
			svg.circle({
				'cx': 0,
				'cy': 0,
				'r': 20 * s,
				'stroke-width': 0.5 * this.strokeWidth,
				'stroke': node.attr('color'),
				'fill': 'transparent',
			}),
			(instantiate
				? null
				: svg.circle({
					'cx': 0,
					'cy': 0,
					'r': 20 * s - 1.5 * this.strokeWidth,
					'stroke-width': 0.5 * this.strokeWidth,
					'stroke': node.attr('color'),
					'fill': 'transparent',
				})
			),
			svg.path({
				"d": new GSvgPath().scale(0.6 * s)
					.M(  0.0, -20.0)
					.L( 19.0,  -6.2)
					.L( 11.8,  16.2)
					.L(-11.8,  16.2)
					.L(-19.0,  -6.2)
					.Z(),
				"fill": "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderParallelEventSymbol(svg: GSvg, node: GNode, s: number): any[]
	{
		return [
			svg.circle({
				'cx': 0,
				'cy': 0,
				'r': 20 * s,
				'stroke-width': 0.5 * this.strokeWidth,
				'stroke': node.attr('color'),
				'fill': 'transparent',
			}),
			svg.path({
				"d": new GSvgPath().scale(0.6 * s)
					.M(  4, -20)
					.L(  4,  -4)
					.L( 20,  -4)
					.L( 20,   4)
					.L(  4,   4)
					.L(  4,  20)
					.L( -4,  20)
					.L( -4,   4)
					.L(-20,   4)
					.L(-20,  -4)
					.L( -4,  -4)
					.L( -4, -20)
					.Z(),
				"fill": "none",
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderComplexSymbol(svg: GSvg, node: GNode, s: number): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
					.M(-3, -20).v(12.7).l(-9, -9).l(-4.2, 4.2).l(9, 9)
					.H(-20).v(6).h(12.7).l(-9, 9).l(4.2, 4.2).l(9, -9)
					.V(20).H(3).V(7.2).L(12, 16.2).L(16.2, 12).L(7.2, 3)
					.H(20).V(-3).H(7.2).l(9, -9).l(-4.2, -4.2).l(-9, 9)
					.V(-20).Z(), 
				"fill": node.attr('color'),
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}


	protected renderParallelSymbol(svg: GSvg, node: GNode, s: number): any[]
	{
		return [
			svg.path({
				"d": new GSvgPath().scale(s)
					.M(  3, -20)
					.L(  3,  -3)
					.L( 20,  -3)
					.L( 20,   3)
					.L(  3,   3)
					.L(  3,  20)
					.L( -3,  20)
					.L( -3,   3)
					.L(-20,   3)
					.L(-20,  -3)
					.L( -3,  -3)
					.L( -3, -20)
					.Z(),
				"fill": node.attr('color'),
				"stroke-width": 0.5 * this.strokeWidth,
				"stroke": node.attr('color'),
			}),
		];
	}

}

