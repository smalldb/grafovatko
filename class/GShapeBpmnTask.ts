/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GNode from './GNode';
import GShapePath from './GShapePath';
import GSvgPath from './GSvgPath';


export default class GShapeBpmnTask extends GShapePath
{
	public readonly minHeightEm: number = 4.5;
	public readonly minWidthEm: number = 9;

	protected getSvgPath(node: GNode): GSvgPath
	{
		let s = node.getSize();
		let r = this.get1em(node);
		let h = s.height / 2;
		let w = s.width / 2;

		// Ends of straight line before the arc
		let ew = s.width / 2 - r;
		let eh = s.height / 2 - r;

		return new GSvgPath(+ew, -h)
			.A(r, r, 0, false, true, +w, -eh).L(+w, +eh)
			.A(r, r, 0, false, true, +ew, +h).L(-ew, +h)
			.A(r, r, 0, false, true, -w, +eh).L(-w, -eh)
			.A(r, r, 0, false, true, -ew, -h).Z();
	}
}

