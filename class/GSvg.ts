/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * Hyperscript wrapper to produce valid SVG elements
 */
export default class GSvg
{
	public readonly h: any;

	constructor(h: any)
	{
		this.h = h;
	}

	public el(tagName: string, attributes: {[attr: string]: any}, children: any[] = [])
	{
		return this.h(tagName, {
			"namespace": "http://www.w3.org/2000/svg",
			"attributes": attributes
		}, children);
	}

	// Shape elements
	public circle   (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('circle',   attributes, children); }
	public ellipse  (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('ellipse',  attributes, children); }
	public line     (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('line',     attributes, children); }
	public mesh     (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('mesh',     attributes, children); }
	public path     (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('path',     attributes, children); }
	public polygon  (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('polygon',  attributes, children); }
	public polyline (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('polyline', attributes, children); }
	public rect     (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('rect',     attributes, children); }

	// Structural elements
	public g        (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('g',        attributes, children); }
	public svg      (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('svg',      attributes, children); }

	// Text
	public text     (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('text',     attributes, children); }
	public tspan    (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('tspan',    attributes, children); }
	public title    (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('title',    attributes, children); }

	public image    (attributes: {[attr: string]: any}, children: any[] = []) { return this.el('image',    attributes, children); }

}

