/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GGraph from './GGraph';
import GLayout from './GLayout';
import GPoint from './GPoint';


export default class GLayoutRandom extends GLayout
{

	public apply(graph: GGraph)
	{
		let w = this.options.width || 500;
		let h = this.options.height || 500;

		for (let n of graph.nodes) {
			n.setPosition(new GPoint(w * Math.random(), h * Math.random(), graph));
		}
	}

}

