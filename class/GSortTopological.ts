/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GGraph from './GGraph';
import GNode from './GNode';
import GEdge from './GEdge';

type InteractionMatrix = { [start: string]: { [end: string]: number }};

export default class GSortTopological
{
	private graph: GGraph;

	public constructor(graph: GGraph)
	{
		this.graph = graph;
	}


	public sort()
	{
		if (this.graph.nodes.length == 0) {
			return this.graph.nodes;
		}

		let roots = this.findRoots(this.graph.nodes);

		return this.bfs(roots);
	}


	private findRoots(nodes: GNode[]): GNode[]
	{
		let rootList : GNode[] = [];
		let otherList : GNode[] = [];

		for (let n of nodes) {
			let hasParents = false;
			for (let e of n.connectedEdges) {
				let start: GNode = (e as GEdge).start;
				let end: GNode = (e as GEdge).end;
				if (start.graph === n.graph && end.graph === n.graph && start !== n && end === n) {
					hasParents = true;
				}
			}
			if (hasParents) {
				otherList.push(n);
			} else {
				rootList.push(n);
			}
		}

		return rootList.concat(otherList);
	}

	private bfs(roots: GNode[]): GNode[]
	{
		let sortedList : GNode[] = [];

		let queue : GNode[] = [];
		let visited = {};

		for (let first of roots) {
			if (!visited[first.id]) {
				visited[first.id] = true;
				queue.push(first);

				while (queue.length > 0) {
					let n = queue.shift();

					sortedList.push(n);

					for (let e of n.connectedEdges) {
						let start: GNode = (e as GEdge).start;
						let end: GNode = (e as GEdge).end;
						if (start.graph === n.graph && end.graph === n.graph && start === n && end !== n && !visited[end.id]) {
							visited[end.id] = true;
							queue.push(end);
						}
					}
				}
			}
		}

		return sortedList;
	}

}

