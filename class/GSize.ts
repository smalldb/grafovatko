/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GBoundingBox from './GBoundingBox';
import GPoint from "./GPoint";


export default class GSize
{
	public readonly width: number;
	public readonly height: number;

	
	constructor(width?: number, height?: number)
	{
		this.width = width;
		this.height = (height === undefined ? width : height);
	}


	public placeAt(p: GPoint): GBoundingBox
	{
		if (!this.isValid()) {
			throw new Error("Can't center an invalid GSize.");
		}

		let w = this.width / 2;
		let h = this.height / 2;
		return new GBoundingBox(p.x - w, p.y - h, p.x + w, p.y + h, p.ref);
	}


	public isValid(): boolean
	{
		return this.width < Infinity && this.height < Infinity;
	}


	public toString(): string
	{
		return this.width.toFixed(1) + 'x' + this.height.toFixed(1);
	}


	public getLonger(): number
	{
		return Math.max(this.width, this.height);
	}


	public getShorter(): number
	{
		return Math.min(this.width, this.height);
	}


	public fit(that: GSize): GSize
	{
		return new GSize(Math.max(this.width, that.width), Math.max(this.height, that.height));
	}

}

