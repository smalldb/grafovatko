/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GGraph from './GGraph';
import GPoint from './GPoint';
import GSize from "./GSize";
import GTransformation from './GTransformation';


export default class GBoundingBox
{
	public tlx: number;
	public tly: number;
	public brx: number;
	public bry: number;
	public readonly ref: GGraph;


	constructor(tlx?: number, tly?: number, brx?: number, bry?: number, ref?: GGraph)
	{
		if (tlx < Infinity && tly < Infinity && brx < Infinity && bry < Infinity) {
			this.tlx = Math.min(tlx, brx);
			this.tly = Math.min(tly, bry);
			this.brx = Math.max(tlx, brx);
			this.bry = Math.max(tly, bry);
		} else if (tlx !== undefined || tly !== undefined || brx !== undefined || bry !== undefined) {
			throw new Error("All or none of the parameters must be specified.");
		}
		this.ref = ref;
	}


	public toRef(destRef: GGraph): GBoundingBox
	{
		if (this.ref === destRef || destRef === undefined) {
			return this;
		} else if (this.ref === undefined) {
			throw new Error("Can't convert to another reference. - BoundingBox reference is not defined.");
		} else {
			return this.transform(this.ref.getAbsTransformation().diff(destRef.getAbsTransformation()), destRef);
		}
	}


	public isValid(): boolean
	{
		return this.tlx < Infinity && this.tly < Infinity && this.brx < Infinity && this.bry < Infinity;
	}


	public transform(tr: GTransformation, destRef?: GGraph): GBoundingBox
	{
		return new GBoundingBox(this.tlx + tr.x, this.tly + tr.y, this.brx + tr.x, this.bry + tr.y, destRef === undefined ? destRef : this.ref);
	}


	public expand(bb: GBoundingBox): GBoundingBox
	{
		if (bb.isValid()) {
			if (bb.ref !== this.ref) {
				bb = bb.toRef(this.ref);
			}
			if (this.isValid()) {
				this.tlx = Math.min(this.tlx, bb.tlx);
				this.tly = Math.min(this.tly, bb.tly);
				this.brx = Math.max(this.brx, bb.brx);
				this.bry = Math.max(this.bry, bb.bry);
			} else {
				this.tlx = bb.tlx;
				this.tly = bb.tly;
				this.brx = bb.brx;
				this.bry = bb.bry;
			}
		}

		return this;
	}


	public addPoint(p: GPoint): GBoundingBox
	{
		if (p.ref !== this.ref) {
			p = p.toRef(this.ref);
		}
		if (this.isValid()) {
			this.tlx = Math.min(this.tlx, p.x);
			this.tly = Math.min(this.tly, p.y);
			this.brx = Math.max(this.brx, p.x);
			this.bry = Math.max(this.bry, p.y);
		} else {
			this.tlx = p.x;
			this.tly = p.y;
			this.brx = p.x;
			this.bry = p.y;
		}
		return this;
	}


	public addMargin(margin_tlx: number, margin_tly?: number, margin_brx?: number, margin_bry?: number): GBoundingBox
	{
		if (!this.isValid()) {
			throw new Error("Can't add margin to an invalid GBoundingBox.");
		}

		if (margin_tly === undefined) {
			margin_tly = margin_tlx;
		}
		if (margin_brx === undefined) {
			margin_brx = margin_tlx;
		}
		if (margin_bry === undefined) {
			margin_bry = margin_tly;
		}

		if (!(margin_tlx < Infinity && margin_tly < Infinity && margin_brx < Infinity && margin_bry < Infinity)) {
			throw new Error("Can't add invalid margin.");
		}

		this.tlx -= margin_tlx;
		this.tly -= margin_tly;
		this.brx += margin_brx;
		this.bry += margin_bry;

		return this;
	}


	public getCenteringTransformation(): GTransformation
	{
		return new GTransformation(-(this.tlx + this.brx) / 2, -(this.tly + this.bry) / 2);
	}


	public getTopLeftTransformation(): GTransformation
	{
		return new GTransformation(-this.tlx, -this.tly);
	}


	public centerAt(p: GPoint): GBoundingBox
	{
		if (!this.isValid()) {
			throw new Error("Can't center an invalid GBoundingBox.");
		}

		if (p.ref !== this.ref) {
			p = p.toRef(this.ref);
		}
		let w = (this.brx - this.tlx) / 2;
		let h = (this.bry - this.tly) / 2;
		return new GBoundingBox(p.x - w, p.y - h, p.x + w, p.y + h, this.ref);
	}


	public getCenter(): GPoint
	{
		return new GPoint((this.tlx + this.brx) / 2, (this.tly + this.bry) / 2, this.ref);
	}


	public getWidth(): number
	{
		return this.brx - this.tlx;
	}


	public getHeight(): number
	{
		return this.bry - this.tly;
	}


	public getBoundingBox(): GBoundingBox
	{
		return this;
	}


	public getSize(): GSize
	{
		return new GSize(this.brx - this.tlx, this.bry - this.tly);
	}


	public getAspectRatio(): number
	{
		return (this.bry != this.tly) ? Math.abs((this.brx - this.tlx) / (this.bry - this.tly)) : Infinity;
	}

	public toString(): string
	{
		return this.tlx.toFixed(1) + ',' + this.tly.toFixed(1)
			+ " … " + this.brx.toFixed(1) + ',' + this.bry.toFixed(1)
			+ (this.ref ? ' + ' + this.ref.getAbsTransformation().toString() : '');
	}

}

