/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GJsonShape from './GJsonShape';
import GShape from './GShape';
import GShapeLibrary from './GShapeLibrary';


export default class GJsonShapeLibrary extends GShapeLibrary
{
	protected jsonShapeData: any = {};

	protected createShape(name: string): GShape
	{
		if (name in this.jsonShapeData) {
			return new GJsonShape(this.jsonShapeData[name]);
		} else {
			return undefined;
		}
	}


	public listShapes(): string[]
	{
		return Object.keys(this.jsonShapeData);
	}

}

