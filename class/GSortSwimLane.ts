/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GGraph from './GGraph';
import GNode from './GNode';

type InteractionMatrix = { [start: string]: { [end: string]: number }};

export default class GSortSwimLane
{
	private graph: GGraph;

	public constructor(graph: GGraph)
	{
		this.graph = graph;
	}


	public sort()
	{
		let interactions = this.countInteractions(this.graph);

		//for (let i = 0; i < this.graph.nodes.length; i++) {
		//	console.log(this.graph.nodes[i].id, this.calculateBarycenter(interactions, this.graph.nodes[i]));
		//}

		if (this.graph.nodes.length > 8) {
			console.warn('GSortSwimLane: Too many nodes for brute-force sort. Leaving nodes unsorted.');
			return this.graph.nodes;
		}

		let min_crossings = this.countCrossings(interactions, this.graph.nodes);
		let min_list = this.graph.nodes;

		this.permutateList(this.graph.nodes, (nodeList: GNode[]) => {
			let c = this.countCrossings(interactions, nodeList);
			if (c < min_crossings) {
				min_crossings = c;
				min_list = nodeList;
			}
		});

		/*
		console.info('Interactions:');
		console.table(interactions);
		console.log('Initial crossings:', this.countCrossings(interactions, this.graph.nodes));
		console.log('Minimal crossings:', this.countCrossings(interactions, min_list));
		// */

		return min_list;
	}


	public permutateList(nodeList: GNode[], callback: (nodeList: GNode[]) => void)
	{
		const permute = (arr, m = []) => {
			if (arr.length === 0) {
				callback(m);
				perm_count++;
			} else {
				for (let i = 0; i < arr.length; i++) {
					let curr = arr.slice();
					let next = curr.splice(i, 1);
					permute(curr.slice(), m.concat(next))
				}
			}
		};

		let perm_count = 0;
		permute(nodeList);
		console.log('GSortSwimLane::permutateList(): Explored permutations:', perm_count);
	}


	/**
	 * Count how many times a swim lane is crossed by an edge not starting nor ending in it.
	 */
	private countCrossings(interactions: InteractionMatrix, nodeList: GNode[]): number
	{
		let crossings = 0;

		for (let i = 0; i < nodeList.length - 2; i++) {
			let ni = nodeList[i];
			for (let j = i + 2; j < nodeList.length; j++) {
				let nj = nodeList[j];
				crossings += (j - i - 1) * (interactions[ni.id][nj.id] + interactions[nj.id][ni.id]);
			}
		}

		return crossings;
	}


	/**
	 * Compute barycenter of a node.
	 *
	 * Since we are in one-dimensional space, result is new position -
	 * index in the list.
	 */
	private calculateBarycenter(interactions: InteractionMatrix, node: GNode): number
	{
		// Retrieve positions of all nodes
		let positions = {};
		for (let i = 0; i < this.graph.nodes.length; i++) {
			positions[this.graph.nodes[i].id] = i;
		}

		// Calculate the 1D barycenter
		let bc_sum = 0;
		let bc_cnt = 0;
		for (let n_id in interactions[node.id]) {
			let int_count = interactions[node.id][n_id] + interactions[n_id][node.id];
			let pos = positions[n_id];
			bc_sum += int_count * pos;
			bc_cnt += int_count;
		}

		console.log('BC:', node.id, bc_sum, bc_cnt, bc_sum / bc_cnt);
		return bc_sum / bc_cnt;
	}


	/**
	 * Count edges between two nodes (including nested nodes in that nodes).
	 */
	private countInteractions(graph: GGraph): InteractionMatrix
	{
		let interactions: InteractionMatrix = {};

		// Prepare interactions matrix
		graph.forEachNode((n) => {
			interactions[n.id] = {};
			graph.forEachNode(function (nn) {
				interactions[n.id][nn.id] = 0;
			});
		});

		// Count edges between nodes (no need for recursion, only edges at top level can cross nodes)
		graph.forEachEdge((e) => {
			let start: GNode = this.findContainingNode(graph, e.start);
			let end: GNode = this.findContainingNode(graph, e.end);
			if (start.graph === graph && end.graph === graph) {
				interactions[start.id][end.id]++;
			}
		});

		return interactions;
	}


	/**
	 * Find top-level node in the graph for given nested node
	 */
	private findContainingNode(graph: GGraph, node: GNode): GNode
	{
		let n = node;
		while (n && n.graph !== graph && n.graph.parentNode) {
			n = n.graph.parentNode;
		}
		return n;
	}

}

