/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GNode from './GNode';
import GShapePath from './GShapePath';
import GSvgPath from './GSvgPath';


export default class GShapeRound extends GShapePath
{
	
	protected getSvgPath(node: GNode): GSvgPath
	{
		let s = node.getSize();
		let w = s.width / 2;
		let h = s.height / 2;

		if (w > h) {
			let r = h;
			let e = w - r;
			return new GSvgPath(+e, -h)
				.A(r, r, 0, true, true, +e, +h)
				.L(-e, +h)
				.A(r, r, 0, true, true, -e, -h)
				.Z();
		} else {
			let r = w;
			let e = h - r;
			return new GSvgPath(-w, -e)
				.A(r, r, 0, true, false, +w, -e)
				.L(+w, +e)
				.A(r, r, 0, true, false, -w, +e)
				.Z();
		}

	}

}

