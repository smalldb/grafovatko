/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GGraph from './GGraph';


export default class GLayout
{
	protected options;

	
	public constructor(options = {})
	{
		this.options = options || {};
	}


	protected option(optionName: string, defaultValue: any)
	{
		let val = this.options[optionName];
		return val !== undefined && val !== null ? val : defaultValue;
	}


	public apply(graph: GGraph)
	{
		// NOP layout
	}

}

