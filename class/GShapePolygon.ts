/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GNode from './GNode';
import GShape from './GShape';
import GSvg from './GSvg';


export default class GShapePolygon extends GShape
{

	protected getSvgPolygonPoints(node: GNode): number[][]
	{
		let points = node.attr('points');
		if (points) {
			return points;
		} else {
			// Fallback to a square
			let s = node.getSize();
			let w = s.width / 2;
			let h = s.height / 2;
			return [
				[ -w, -h ],
				[ -w, +h ],
				[ +w, +h ],
				[ +w, -h ],
			];
		}
	}


	public renderElements(svg: GSvg, node: GNode): any[]
	{
		return [
			this.renderOutlineElement(svg, node),
			this.renderLabel(svg, node),
		];
	}


	protected renderOutlineElement(svg: GSvg, node: GNode): any[]
	{
		return svg.polygon({
			'points': this.getSvgPolygonPoints(node).map((p) => p.join(',')).join(' '),
			'stroke-width': this.strokeWidth,
			'stroke': node.attr('color'),
			'fill': node.attr('fill'),
		});
	}

	protected getSvgIntersectionsShapes(node: GNode)
	{
		return [
			[ "polygon", { points: this.getSvgPolygonPoints(node).map((p) => p.join(',')).join(' ') }],
		];
	}

}
