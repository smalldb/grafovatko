/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GNode from './GNode';
import GShape from './GShape';
import GSvg from './GSvg';

export default class GJsonShape extends GShape
{
	protected jsonShapeData: any;
	
	constructor(jsonShapeData: any)
	{
		super();
		this.jsonShapeData = jsonShapeData;
	}


	/**
	 * Render elements of the shape. The render() will add the wrapping `<g>`.
	 *
	 * @see render()
	 *
	 * @param svg Hyperscript-compatible element factory/renderer.
	 * @param node Node to render.
	 */
	protected renderElements(svg: GSvg, node: GNode): any[]
	{
		//console.log(this.jsonShapeData.svg);
		return this.jsonShapeData.svg.map((e) => this.renderJsonToHyperscript(svg, node, e));
	}


	protected renderJsonToHyperscript(svg: GSvg, node: GNode, jsonElement: any): any[]
	{
		let e = jsonElement;
		return svg.el(e[0], e[1], e[2] && e[2].length > 0 ? e[2].map((el) => this.renderJsonToHyperscript(svg, node, el)) : []);
	}

}

