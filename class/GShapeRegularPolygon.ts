/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GNode from './GNode';
import GShapePolygon from './GShapePolygon';


export default class GShapeRegularPolygon extends GShapePolygon
{

	protected getEdgeCount(node: GNode): number
	{
		return node.attr('n', 6);
	}


	protected getSvgPolygonPoints(node: GNode): number[][]
	{
		let attrPoints = node.attr('points');
		if (attrPoints) {
			return attrPoints;
		} else {
			let s = node.getSize();
			let r = Math.max(s.width, s.height) / 2;
			let n = this.getEdgeCount(node);
			let a = 2 * Math.PI / n;
			let rot = a * node.attr('rotation', 0.5);
			let points = [];

			for (let i = 0; i < n; i++) {
				points.push([
					(r * Math.sin(i * a + rot)).toFixed(1),
					(r * Math.cos(i * a + rot)).toFixed(1),
				]);
			}

			return points;
		}
	}

}

