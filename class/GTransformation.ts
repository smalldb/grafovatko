/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


/**
 * 2D transformation.
 *
 * Using translate-only matrix: 
 * 	1 0 x
 * 	0 1 y
 * 	0 0 1
 */
export default class GTransformation
{
	public readonly x: number;
	public readonly y: number;

	
	constructor(x: number = 0, y: number = 0)
	{
		this.x = x;
		this.y = y;
	}


	invert(): GTransformation
	{
		return new GTransformation(-this.x, -this.y);
	}


	append(that: GTransformation): GTransformation
	{
		return new GTransformation(this.x + that.x, this.y + that.y);
	}


	appendInverted(that: GTransformation): GTransformation
	{
		return new GTransformation(this.x - that.x, this.y - that.y);
	}


	diff(that: GTransformation): GTransformation
	{
		return new GTransformation(this.x - that.x, this.y - that.y);
	}


	translate(x: number, y: number): GTransformation
	{
		return new GTransformation(this.x + x, this.y + y);
	}


	toCss(): string
	{
		return "translate(" + this.x.toFixed(1) + ',' + this.y.toFixed(1) + ')';
	}


	toString(): string
	{
		return this.toCss();
	}

}

