/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GBoundingBox from './GBoundingBox';
import GEdge from './GElement';
import GElement from './GElement';
import GGraph from './GGraph';
import GPoint from './GPoint';
import GShape from './GShape';
import GSize from "./GSize";
import GTransformation from './GTransformation';
import GVector from './GVector';


export default class GNode extends GElement
{
	private position: GPoint;
	private size: GSize;
	private boundingBox: GBoundingBox;
	private shape: GShape;

	public connectedEdges: GEdge[] = [];
	private nestedGraph: GGraph = null;


	constructor(graph: GGraph, id: string, attrs: any)
	{
		super(graph, id, attrs);

		let x = this.attr('x');
		let y = this.attr('y');
		if (x !== undefined && y !== undefined && x !== null && y !== null) {
			this.position = new GPoint(x, y, this.graph);
		}
	}


	public getAttributeDefaultValue(attrName: string): any
	{
		return this.graph.getNodeAttributeDefaultValue(attrName);
	}


	public connectEdge(edge: GEdge)
	{
		this.connectedEdges.push(edge);
	}


	public getNestedGraph(): GGraph
	{
		if (this.nestedGraph === null) {
			this.nestedGraph = this.graph.createNestedGraph(this);
		}
		return this.nestedGraph;
	}


	public hasNestedGraph(): boolean
	{
		return this.nestedGraph !== null && this.nestedGraph.nodes.length > 0;
	}


	public getShapeName(): string
	{
		return this.attr('shape', this.nestedGraph === null ? 'rect' : 'nested_graph');
	}


	public getShape(): GShape
	{
		return this.shape;
	}


	public setShape(shape: GShape): GNode
	{
		this.shape = shape;
		this.size = undefined;
		this.boundingBox = undefined;
		return this;
	}

	
	public render(h: any): any
	{
		if (this.boundingBox) {
			return this.shape.render(h, this);
		} else {
			throw new Error("Layout not applied to GNode.");
		}
	}


	public applyLayout()
	{
		if (!this.shape) {
			throw new Error("Shape not assigned to GNode.");
		}

		if (this.hasNestedGraph()) {
			this.getNestedGraph().applyLayout();
		}

		this.size = this.shape.getMinSize(this);
		if (!this.size.isValid()) {
			throw new Error("Failed to calculate node size.");
		}

		this.boundingBox = undefined;
	}


	public setPosition(p: GPoint): GNode
	{
		this.position = p;
		this.boundingBox = undefined;
		return this;
	}


	public getPosition(): GPoint
	{
		return this.position;
	}


	public getSize(): GSize
	{
		return this.size;
	}


	public getMargin(): number
	{
		return this.shape.getMargin(this);
	}


	public getBoundingBox(): GBoundingBox
	{
		if (this.boundingBox !== undefined) {
			return this.boundingBox;
		} else if (this.position !== undefined && this.size !== undefined) {
			return (this.boundingBox = this.size.placeAt(this.position));
		} else {
			throw new Error("Bounding box not available - position or size not known.");
		}
	}


	public getConnectorVector(sourcePoint: GPoint, connectorName?: string): GVector
	{
		return this.shape.getConnectorVector(this, sourcePoint, connectorName);
	}


	public getNestedGraphTransformation(): GTransformation
	{
		return this.shape.getNestedGraphTransformation(this);
	}

}

