/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GPoint from './GPoint';
import GGraph from './GGraph';

export default class GVector
{
	public readonly start: GPoint;
	public readonly end: GPoint;
	public readonly ref: GGraph;


	constructor(start: GPoint, end: GPoint, ref?: GGraph)
	{
		if (ref) {
			this.start = start.toRef(ref);
			this.end = end.toRef(ref);
			this.ref = ref;
		} else if (start.ref === end.ref) {
			this.start = start;
			this.end = end;
			this.ref = start.ref;
		} else {
			if (!start.ref) {
				throw "Start point has no reference point and no vector reference point is specified.";
			}
			if (!end.ref) {
				throw "End point has no reference point and no vector reference point is specified.";
			}
			throw "Start and end reference points do not match and vector reference point is not specified.";
		}
	}


	toRef(newRef: GGraph): GVector
	{
		if (this.ref === newRef) {
			return this;
		} else {
			return new GVector(this.start, this.end, newRef);
		}
	}


	toString(): string
	{
		return "[" + this.start.toString() + "]→[" + this.end.toString() + "]" + (this.ref ? " + " + this.ref.getAbsTransformation().toString() : "");
	}


	invert(): GVector
	{
		return new GVector(this.end, this.start, this.ref);
	}


	size(): number
	{
		let dx = this.end.x - this.start.x;
		let dy = this.end.y - this.start.y;
		return Math.sqrt(dx * dx + dy * dy);
	}

	angleRad(): number
	{
		return Math.atan2(this.end.y - this.start.y, this.end.x - this.start.x);
	}


	angleDeg(): number
	{
		return 180. * Math.atan2(this.end.y - this.start.y, this.end.x - this.start.x) / Math.PI;
	}


	addToSize(add: number): GVector
	{
		var s = this.size() + add;
		var a = this.angleRad();

		return new GVector(this.start, new GPoint(this.start.x + s * Math.cos(a), this.start.y + s * Math.sin(a), this.start.ref), this.ref);
	}


	getPointFromStart(distance: number): GPoint
	{
		var a = this.angleRad();
		return new GPoint(this.start.x + distance * Math.cos(a), this.start.y + distance * Math.sin(a), this.ref)
	}

	getPointFromEnd(distance: number): GPoint
	{
		var a = this.angleRad();
		return new GPoint(this.end.x - distance * Math.cos(a), this.end.y - distance * Math.sin(a), this.ref)
	}

}

