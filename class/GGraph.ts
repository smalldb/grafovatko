/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GBoundingBox from './GBoundingBox';
import GEdge from './GEdge';
import GLayout from './GLayout';
import GLayoutColumn from './GLayoutColumn';
import GLayoutDagre from './GLayoutDagre';
import GLayoutRandom from './GLayoutRandom';
import GLayoutRow from './GLayoutRow';
import GNode from './GNode';
import GPoint from './GPoint';
import GShape from './GShape';
import GShapeLibrary from './GShapeLibrary';
import GTransformation from './GTransformation';


export default class GGraph
{
	public readonly rootGraph: GGraph;
	public readonly parentNode: GNode;
	public nodes: GNode[];
	public edges: GEdge[];
	public layout: GLayout;
	public extraSvg: any[];
	private absTransformation: GTransformation;
	private elementCount: number;
	private elementById: { [ id: string ]: GNode | GEdge };
	private classDefaults: {[className: string]: {[propertyName: string]: any}} = {};

	constructor(rootGraph?: GGraph, parentNode?: GNode)
	{
		if (rootGraph) {
			this.rootGraph = rootGraph;
		} else {
			this.elementById = {};
			this.elementCount = 0;
			this.rootGraph = this;
		}

		this.parentNode = parentNode;
		this.nodes = [];
		this.edges = [];
	}


	public createNestedGraph(parentNode: GNode): GGraph
	{
		return new GGraph(this.rootGraph, parentNode);
	}

	
	public getEdgeById(id: string): GEdge
	{
		if (id in this.rootGraph.elementById) {
			let edge: GNode|GEdge = this.rootGraph.elementById[id];
			if (edge instanceof GEdge) {
				return this.rootGraph.elementById[id] as GEdge;
			} else {
				throw new Error("Requested edge " + id + " is a node (conflicting IDs).");
			}
		} else {
			throw new Error("Edge not found: " + id);
		}
	}


	public forEachEdge(fn: (node: GEdge) => boolean | void): void
	{
		for (let e of this.edges) {
			if (fn(e) === false) {
				break;
			}
		}
	}


	public mapEdges(fn: (edge: GEdge) => any): any[]
	{
		return this.edges.map(fn);
	}


	public addNode(id: string, data: any)
	{
		if (id === undefined) {
			id = '_' + (this.rootGraph.elementCount++);
		}

		let node = new GNode(this, id, data);
		this.add(node);
		return node;
	}


	public hasElement(id: string): boolean
	{
		return (id in this.rootGraph.elementById);
	}


	public getNodeById(id: string): GNode
	{
		if (id in this.rootGraph.elementById) {
			let node: GNode|GEdge = this.rootGraph.elementById[id];
			if (node instanceof GNode) {
				return this.rootGraph.elementById[id] as GNode;
			} else {
				throw new Error("Requested node " + id + " is an edge (conflicting IDs).");
			}
		} else {
			throw new Error("Node not found: " + id);
		}
	}


	public forEachNode(fn: (node: GNode) => boolean | void): void
	{
		for (let n of this.nodes) {
			if (fn(n) === false) {
				break;
			}
		}
	}


	public mapNodes(fn: (node: GNode) => any): any[]
	{
		return this.nodes.map(fn);
	}


	public addEdge(id: string, data: any, start: GNode | string, end: GNode | string, points: GPoint[] = [])
	{
		if (id === undefined) {
			id = '_' + (this.rootGraph.elementCount++);
		}

		if (!(start instanceof GNode)) {
			start = this.getNodeById(start);
		}

		if (!(end instanceof GNode)) {
			end = this.getNodeById(end);
		}

		let edge = new GEdge(this, id, data, start, end, points);
		this.add(edge);
		start.connectEdge(edge);
		end.connectEdge(edge);
		return edge;
	}


	public add(element: GNode | GEdge)
	{
		if (element.id in this.rootGraph.elementById) {
			throw new Error("Duplicate ID: " + element.id);
		}

		this.rootGraph.elementById[element.id] = element;

		if (element instanceof GNode) {
			this.nodes.push(element);
		}
		else if (element instanceof GEdge) {
			
			this.edges.push(element);
		}

		return this;
	}


	public getBoundingBox(): GBoundingBox
	{
		let bb = new GBoundingBox(undefined, undefined, undefined, undefined, this);

		// Add nodes
		for (let n of this.nodes) {
			bb.expand(n.getBoundingBox());
		}

		// Add edges
		for (let e of this.edges) {
			bb.expand(e.getBoundingBox());
		}

		// Debug: Show bounding box calculations
		/*
		console.group("Get Graph Bounding Box:");
		console.log("%cLayout:", "font-weight: bold;", this.layout);
		for (let e of this.edges) {
			if (e.start.graph !== this || e.end.graph !== this) {
				let ebb = e.getBoundingBox().toRef(this);
				console.log("%cEdge:", "font-weight: bold;", e.id,
					"\n  Start:", e.start.getPosition().toString(),
					"\n  End:", e.end.getPosition().toString(),
					"\n  BB:", ebb.toString(), "(width:", ebb.getWidth(), ")");
			}
		}
		console.log("Whole BB:", bb.toString());
		console.groupEnd();
		// */

		return bb;
	}


	public resolveShapes(shapeLibraries: GShapeLibrary[])
	{
		for (let n of this.nodes) {
			let shapeName = n.getShapeName();
			let shape: GShape;
			for (let shapeLibrary of shapeLibraries) {
				shape = shapeLibrary.getShape(shapeName);
				if (shape) {
					break;
				}
			}
			if (shape) {
				n.setShape(shape);
				if (n.hasNestedGraph()) {
					n.getNestedGraph().resolveShapes(shapeLibraries);
				}
			} else {
				throw new Error("Failed to resolve shape: " + shapeName);
			}
		}
	}


	public getNodeAttributeDefaultValue(attrName: string): any
	{
		switch (attrName) {
			case 'color':
				return '#000';
			case 'fill':
				return 'none';
			default:
				return undefined;
		}
	}


	public getEdgeAttributeDefaultValue(attrName: string): any
	{
		switch (attrName) {
			case 'color':
				return '#000';
			default:
				return undefined;
		}
	}


	public setLayout(layout: string | GLayout, options?: any)
	{
		if (layout instanceof GLayout) {
			return this.layout = layout;
		}

		switch (layout) {
			case 'null':
			case 'none':
				return this.layout = new GLayout(options);
			case 'random':
				return this.layout = new GLayoutRandom(options);
			case 'default':
			case 'dagre':
				return this.layout = new GLayoutDagre(options);
			case 'row':
				return this.layout = new GLayoutRow(options);
			case 'column':
				return this.layout = new GLayoutColumn(options);
			default:
				throw new Error("Unknown layout: " + layout);
		}
	}


	public applyLayout()
	{
		// Apply layouts to nested nodes first
		this.forEachNode((node: GNode) => {
			node.applyLayout();
		});

		// Apply the layout
		if (this.layout) {
			this.layout.apply(this);
		} else {
			// If we don't have layout, check that we don't need it.
			for (let n of this.nodes) {
				if (n.getPosition() === undefined) {
					throw new Error("Layout not set and node \"" + n.id + "\" is not placed.");
				}
			}
		}

		// Update nested transformations, so we get cross-graph edges right.
		this.calculateTransformations(this.absTransformation);
	}


	public getAbsTransformation(): GTransformation
	{
		/*
		if (!this.absTransformation) {
			throw "Transformations not calculated yet!";
		}
		return this.absTransformation;
		/*/
		return this.absTransformation || new GTransformation(); // FIXME: Thrown an exception if transformation not available. yet
		// */
	}


	public calculateTransformations(transformation?: GTransformation, depth: number = 0)
	{
		this.absTransformation = transformation || new GTransformation();

		// Debug: Dump graph hierarchy with transformations
		/*
		let bb = this.getBoundingBox();
		console.log((<any>'.  ').repeat(depth) + 'Graph: ' + this.absTransformation.toString(), '→', bb.center.toString());
		// */

		// Recursively propagate transformations to all nested graphs
		this.forEachNode((node: GNode) => {
			if (node.hasNestedGraph()) {
				let tr = node.getNestedGraphTransformation();
				node.getNestedGraph().calculateTransformations(this.absTransformation.append(tr), depth + 1);
			}
		});
	}


	public forEachNestedGraph(fn: (graph: GGraph, node: GNode, depth: number) => boolean | void, depth: number = 0): void
	{
		this.forEachNode((node: GNode) => {
			let r;
			if (node.hasNestedGraph()) {
				let nestedGraph = node.getNestedGraph();
				r = fn(nestedGraph, node, depth);
				if (r !== false) {
					nestedGraph.forEachNestedGraph(fn, depth + 1);
				}
			}
			return r;
		});
	}


	public setClassDefaults(className: string, defaults: {[propertyName: string]: any})
	{
		if (!(className in this.classDefaults)) {
			this.classDefaults[className] = defaults;
		} else {
			throw new Error("Class defaults already set for class \"" + className + "\".");
		}
	}


	public getClassDefaults(className: string): {[propertyName: string]: any}
	{
		if (className in this.classDefaults) {
			return this.classDefaults[className];
		} else {
			return undefined;
		}
	}

}

