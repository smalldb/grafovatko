/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import Dagre from 'dagre';
import GGraph from './GGraph';
import GLayout from './GLayout';
import GNode from "./GNode";
import GPoint from './GPoint';


/**
 * Dagre layout
 *
 * Options:
 * 	See https://github.com/cpettitt/dagre/wiki#configuring-the-layout
 *
 * The trick: Each node and edge in Dagre's graph has a label, which is an
 * object. We store reference to Grafovatko's objects in these labels to avoid
 * lookups when storing results.
 *
 * See https://github.com/cpettitt/graphlib/wiki/API-Reference
 */
export default class GLayoutDagre extends GLayout
{

	public apply(graph: GGraph)
	{
		// Create graphlib Graph
		let g = new Dagre.graphlib.Graph({ multigraph: true });
		g.setGraph(this.options);

		// Add nodes
		graph.forEachNode((n: GNode) => {
			let s = n.getSize();
			g.setNode(n.id, {
				gn: n,
				width: Math.round(s.width),
				height: Math.round(s.height),
			});
		});

		// Add edges
		graph.forEachEdge((e) => {
			if (e.start.graph === graph && e.end.graph === graph) {
				g.setEdge(e.start.id, e.end.id, {
					ge: e,
					weight: e.attr('weight', 1),
					label: {width: 100, height: 20},  // FIXME
				}, e.id);
			} else {
				console.warn('GLayoutDagre: Ignoring edge %s (%s -> %s).', e.id, e.start.id, e.end.id);
			}
		});

		// Run layout
		Dagre.layout(g);

		(window as any).g = g;

		// Place GNodes
		g.nodes().forEach((id) => {
			let n = g.node(id);
			if (!n) {
				console.error('WTF? node is missing:', id, graph.getNodeById(id));
				let gn = graph.getNodeById(id);
				gn.setPosition(new GPoint(0, 0, graph));
			} else {
				n.gn.setPosition(new GPoint(Math.round(n.x), Math.round(n.y), graph));
			}
		});

		// Place GEdges (add waypoints)
		g.edges().forEach((vw) => {
			let e = g.edge(vw);
			let gp = [];
			// Place the arrow
			for (let i = 1; i < e.points.length - 1; i++) {
				let p = e.points[i];
				gp.push(new GPoint(Math.round(p.x), Math.round(p.y), graph));
			}
			// Place the label at middle point of the arrow
			if (e.points.length >= 3) {
				let lp = e.points[Math.floor(e.points.length / 2)];
				e.ge.labelPosition = new GPoint(lp.x, lp.y, graph);
			}
			//console.log(JSON.stringify(e.points), JSON.stringify(gp));
			e.ge.points = gp;
		});

	}

}

