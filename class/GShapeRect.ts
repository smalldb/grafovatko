/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GNode from './GNode';
import GShape from './GShape';
import GSvg from './GSvg';


export default class GShapeRect extends GShape
{

	public renderElements(svg: GSvg, node: GNode): any[]
	{
		let s = node.getSize();
		let w = s.width;
		let h = s.height;

		return [
			svg.rect({
				'x': - w / 2,
				'y': - h / 2,
				'width': w,
				'height': h,
				'stroke-width': this.strokeWidth,
				'stroke': node.attr('color'),
				'fill': node.attr('fill'),
			}),
			this.renderLabel(svg, node),
		];
	}


	protected getSvgIntersectionsShapes(node: GNode): any[]
	{
		let s = node.getSize();
		let w = s.width;
		let h = s.height;
		return [
			[ "rect", { x: -w / 2, y: -h / 2, width: w, height: h } ],
		];
	}

}

