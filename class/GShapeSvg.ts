/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GNode from './GNode';
import GShape from './GShape';
import GSvg from './GSvg';
import GSize from "./GSize";
import Base64 from 'base64-js/index.js';
import TextEncoderLite from 'text-encoder-lite/text-encoder-lite.js';

declare var TextEncoder;


export default class GShapeSvg extends GShape
{

	public renderElements(svg: GSvg, node: GNode): any[]
	{
		let svgString = node.attr('svg');

		let s = node.getSize();
		let w = s.width;
		let h = s.height;

		return [
			svg.image({
				href: 'data:image/svg+xml;utf8,' + escape(svgString),
				width: w,
				height: h,
				transform: 'translate(' + (-w/2) + ',' + (-h/2) + ')',
			}),
		];
	}


	public getMinSize(node: GNode): GSize
	{
		let svgString = node.attr('svg');
		let svgDom: any = (new DOMParser()).parseFromString(svgString, "image/svg+xml");
		if (svgDom.documentElement.viewBox) {
			let baseVal: any = svgDom.documentElement.viewBox.baseVal;
			return new GSize(baseVal.width, baseVal.height);
		} else {
			return new GSize(0, 0);
		}
	}


	protected getSvgIntersectionsShapes(node: GNode): any[]
	{
		let s = node.getSize();
		let w = s.width;
		let h = s.height;
		return [
			[ "rect", { x: -w / 2, y: -h / 2, width: w, height: h } ],
		];
	}

}

