/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GGraph from './GGraph';


export default class GElement
{
	public readonly graph: GGraph;
	public readonly id: string;
	protected attrs: any;


	constructor(graph: GGraph, id: string, attrs?: object)
	{
		this.graph = graph;
		this.id = id;
		this.attrs = attrs || {};
	}


	public attr(attrName: string, defaultValue?: any): any
	{
		let attr = this.attrs.hasOwnProperty(attrName) ? this.attrs[attrName] : undefined;

		if (attr !== undefined && attr !== null) {
			return attr;
		} else {
			let def = this.getAttributeDefaultValue(attrName);
			if (def !== undefined) {
				return def;
			} else {
				return defaultValue;
			}
		}
	}


	public setAttr(attrName: string, newValue: any): GElement
	{
		this.attrs[attrName] = newValue;
		return this;
	}


	public getAttributeDefaultValue(attrName: string): any
	{
		return undefined;
	}


}

