/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


import GFallbackShapeLibrary from './GFallbackShapeLibrary';
import GGraph from './GGraph';
import GPoint from './GPoint';
import GRenderer from './GRenderer';
import GShapeLibrary from './GShapeLibrary';
import GSvg from './GSvg';
import GTransformation from './GTransformation';
//import GDiaShapeLibrary from './GDiaShapeLibrary';

import createElement from 'virtual-dom/create-element';
import diff from 'virtual-dom/diff';
import h from 'virtual-dom/h';
import patch from 'virtual-dom/patch';


export default class GGraphView
{
	public readonly svgElement: Element;
	public readonly graph: GGraph;
	public readonly shapeLibraries: GShapeLibrary[];
	public readonly renderer: GRenderer;
	public readonly svg: GSvg;
	protected vTree = null;
	protected vElement = null;
	protected isRendered: boolean = false;


	constructor(svgElement: string | Element, graph?: any)
	{
		// Get SVG element where the graph should be rendered
		if (svgElement instanceof Element) {
			this.svgElement = svgElement;
		} else {
			this.svgElement = document.querySelector(svgElement);
			if (!this.svgElement) {
				throw new Error("SVG element not found: " + svgElement);
			}
		}

		// Get the graph to render
		if (graph instanceof GGraph) {
			this.graph = graph;
		} else if (graph && graph.nodes && graph.edges) {
			this.graph = this.createGraphFromObject(graph);
		} else {
			let graphAttr = this.svgElement.getAttribute('data-graph');
			if (graphAttr) {
				this.graph = this.createGraphFromObject(JSON.parse(graphAttr));
			} else {
				this.graph = new GGraph();
			}
		}

		// Prepare renderers
		this.svg = new GSvg(h);
		this.shapeLibraries = [
			new GShapeLibrary(),
			//new GDiaShapeLibrary(),
			new GFallbackShapeLibrary(),
		];
		this.renderer = new GRenderer();

		// Enqueue initial render
		window.setTimeout(() => {
			this.update();
		}, 0);
	}


	/**
	 * Update svgElement using a new virtual DOM tree.
	 */
	public update()
	{
		if (this.vTree !== null) {
			let newTree = this.render();
			let patches = diff(this.vTree, newTree);
			this.vElement = patch(this.vElement, patches);
			this.vTree = newTree;
		} else {
			this.vTree = this.render();
			this.vElement = createElement(this.vTree);
			this.svgElement.appendChild(this.vElement);
		}
	}


	/**
	 * Render graph into virtual DOM tree
	 */
	public render()
	{
		// Resolve names
		// ... ?

		// Resolve shapes
		this.graph.resolveShapes(this.shapeLibraries);

		// Apply layout
		this.graph.applyLayout();

		// Configure view
		let gbb = this.graph.getBoundingBox();
		if (!gbb.isValid()) {
			throw new Error("Failed to calculate graph bounding box.");
		}

		let width = Math.ceil(gbb.getWidth() + 2).toString();
		let height = Math.ceil(gbb.getHeight() + 2).toString();
		this.svgElement.setAttribute('width', width);
		this.svgElement.setAttribute('height', height);
		this.svgElement.setAttribute('viewBox', "0 0 " + width + " " + height);

		return this.renderer.renderGraph(this.svg, this.graph, new GTransformation(-gbb.tlx, -gbb.tly));
	}

	
	protected createGraphFromObject(obj, graph?: GGraph)
	{
		let g = graph || new GGraph();

		if (obj.extraSvg) {
			g.extraSvg = obj.extraSvg;
		}

		if (obj.layout) {
			g.setLayout(obj.layout, obj.layoutOptions || {});
		}

		if (obj.nodes) {
			let nl = obj.nodes.length;
			for (let i = 0; i < nl; i++) {
				let n = obj.nodes[i];
				let node = g.addNode(n.id, n.attrs || n);
				if (n.graph) {
					this.createGraphFromObject(n.graph, node.getNestedGraph());
				}
			}

		}

		if (obj.edges) {
			let el = obj.edges.length;
			for (let i = 0; i < el; i++) {
				let e = obj.edges[i];
				let points: GPoint[] = e.points ? e.points.map((p) => (p instanceof GPoint ? p : new GPoint(p[0], p[1], g))) : [];
				g.addEdge(e.id, e.attrs || e, e.start, e.end, points);
			}
		}

		return g;
	}

}

