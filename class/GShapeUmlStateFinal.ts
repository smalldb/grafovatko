/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GNode from './GNode';
import GShapeCircle from './GShapeCircle';
import GSize from "./GSize";
import GSvg from './GSvg';


export default class GShapeUmlStateFinal extends GShapeCircle
{
	
	public renderElements(svg: GSvg, node: GNode): any[]
	{
		let r = node.getSize().width / 2;
		return [
			svg.circle({
				'cx': 0,
				'cy': 0,
				'r': r,
				'stroke-width': this.strokeWidth,
				'stroke': node.attr('color'),
				'fill': node.attr('fill'),
			}),
			svg.circle({
				'cx': 0,
				'cy': 0,
				'r': r - 3 * this.strokeWidth,
				'stroke-width': this.strokeWidth,
				'stroke': node.attr('color'),
				'fill': node.attr('color'),
			}),
		];
	}


	public getMinSize(node: GNode): GSize
	{
		return new GSize(1.5 * this.get1em(node) + 3 * this.strokeWidth);
	}

}

