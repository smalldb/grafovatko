/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GNode from './GNode';
import GShape from './GShape';
import GSvg from './GSvg';
import GSvgPath from './GSvgPath';


export default class GShapePath extends GShape
{

	protected getSvgPath(node: GNode): GSvgPath
	{
		// Fallback to a rectangle
		let s = node.getSize();
		let w = s.width / 2;
		let h = s.height / 2;
		return new GSvgPath(-w, -h).L(-w, +h).L(+w, +h).L(+w, -h).Z();
	}


	public renderElements(svg: GSvg, node: GNode): any[]
	{
		return [
			svg.path({
				'd': this.getSvgPath(node),
				'stroke-width': this.strokeWidth,
				'stroke': node.attr('color'),
				'fill': node.attr('fill'),
			}),
			this.renderLabel(svg, node),
		];
	}


	protected getSvgIntersectionsShapes(node: GNode): any[]
	{
		return [
			[ "path", { d: this.getSvgPath(node).toString() }],
		];
	}


}

