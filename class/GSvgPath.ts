/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GPoint from './GPoint';


/**
 * Helper class to build SVG paths.
 *
 * Limitations:
 *   - Does not allow repeating parameters.
 *
 * See also:
 *   - https://www.w3.org/TR/SVG/paths.html#PathDataGeneralInformation
 *   - https://developer.mozilla.org/en/docs/Web/SVG/Tutorial/Paths
 *
 */
export default class GSvgPath
{
	private path: string = '';
	public scaleFactor = 1;

	constructor(x?: number | GPoint, y?: number)
	{
		if (x instanceof GPoint) {
			this.Mp(x);
		} else if (x !== undefined && y !== undefined) {
			this.M(x, y);
		}
	}


	toString(): string
	{
		return this.path;
	}


	protected num(n: number)
	{
		return this.scaleFactor == 1 ? n.toFixed(1) : (this.scaleFactor * n).toFixed(1);
	}


	public scale(s: number)
	{
		this.scaleFactor = s;
		return this;
	}


	Z(): GSvgPath { this.path += ' Z'; return this; }
	z(): GSvgPath { this.path += ' z'; return this; }

	// XY versions

	M(x: number, y: number): GSvgPath { this.path += (this.path === '' ? 'M':' M') + this.num(x) + ',' + this.num(y); return this; }
	m(x: number, y: number): GSvgPath { this.path += (this.path === '' ? 'm':' m') + this.num(x) + ',' + this.num(y); return this; }

	L(x: number, y: number): GSvgPath { this.path += ' L' + this.num(x) + ',' + this.num(y); return this; }
	l(x: number, y: number): GSvgPath { this.path += ' l' + this.num(x) + ',' + this.num(y); return this; }

	H(x: number): GSvgPath { this.path += ' H'+this.num(x); return this; }
	h(x: number): GSvgPath { this.path += ' h'+this.num(x); return this; }

	V(y: number): GSvgPath { this.path += ' V'+this.num(y); return this; }
	v(y: number): GSvgPath { this.path += ' v'+this.num(y); return this; }

	C(x1: number, y1: number, x2: number, y2: number, x: number, y: number): GSvgPath {
		this.path += ' C'+this.num(x1)+','+this.num(y1)+' '+this.num(x2)+','+this.num(y2)+' '+this.num(x)+','+this.num(y);
		return this;
	}
	c(x1: number, y1: number, x2: number, y2: number, x: number, y: number): GSvgPath {
		this.path += ' c'+this.num(x1)+','+this.num(y1)+' '+this.num(x2)+','+this.num(y2)+' '+this.num(x)+','+this.num(y);
		return this;
	}

	S(x2: number, y2: number, x: number, y: number): GSvgPath { this.path += ' S'+this.num(x2)+','+this.num(y2)+' '+this.num(x)+','+this.num(y); return this; }
	s(x2: number, y2: number, x: number, y: number): GSvgPath { this.path += ' s'+this.num(x2)+','+this.num(y2)+' '+this.num(x)+','+this.num(y); return this; }

	Q(x1: number, y1: number, x: number, y: number): GSvgPath { this.path += ' Q'+this.num(x1)+','+this.num(y1)+' '+this.num(x)+','+this.num(y); return this; }
	q(x1: number, y1: number, x: number, y: number): GSvgPath { this.path += ' q'+this.num(x1)+','+this.num(y1)+' '+this.num(x)+','+this.num(y); return this; }

	T(x: number, y: number): GSvgPath { this.path += ' T'+this.num(x)+','+this.num(y); return this; }
	t(x: number, y: number): GSvgPath { this.path += ' t'+this.num(x)+','+this.num(y); return this; }


	A(rx: number, ry: number, x_axis_rotation: number, large_arc_flag: boolean, sweep_flag: boolean, x: number, y: number): GSvgPath
	{
		this.path += ' A' + this.num(rx) + ',' + this.num(ry) + ' ' + x_axis_rotation.toFixed(2)
			+ ' ' + (large_arc_flag ? '1' : '0') + ' ' + (sweep_flag ? '1' : '0')
			+ ' ' + this.num(x) + ',' + this.num(y);
		return this;
	}


	a(rx: number, ry: number, x_axis_rotation: number, large_arc_flag: boolean, sweep_flag: boolean, x: number, y: number): GSvgPath
	{
		this.path += ' a' + this.num(rx) + ',' + this.num(ry) + ' ' + x_axis_rotation.toFixed(2)
			+ ' ' + (large_arc_flag ? '1' : '0') + ' ' + (sweep_flag ? '1' : '0')
			+ ' ' + this.num(x) + ',' + this.num(y);
		return this;
	}


	// GPoint versions
	// ---------------

	Mp(p: GPoint): GSvgPath { this.path += (this.path === '' ? 'M':' M') + this.num(p.x)+','+this.num(p.y); return this; }
	mp(p: GPoint): GSvgPath { this.path += (this.path === '' ? 'm':' m') + this.num(p.x)+','+this.num(p.y); return this; }

	Lp(p: GPoint): GSvgPath { this.path += ' L'+this.num(p.x)+','+this.num(p.y); return this; }
	lp(p: GPoint): GSvgPath { this.path += ' l'+this.num(p.x)+','+this.num(p.y); return this; }

	Cp(p1: GPoint, p2: GPoint, p: GPoint): GSvgPath {
		this.path += ' C'+this.num(p1.x)+','+this.num(p1.y)+' '+this.num(p2.x)+','+this.num(p2.y)+' '+this.num(p.x)+','+this.num(p.y);
		return this;
	}
	cp(p1: GPoint, p2: GPoint, p: GPoint): GSvgPath {
		this.path += ' c'+this.num(p1.x)+','+this.num(p1.y)+' '+this.num(p2.x)+','+this.num(p2.y)+' '+this.num(p.x)+','+this.num(p.y);
		return this;
	}

	Sp(p2: GPoint, p: GPoint): GSvgPath { this.path += ' S'+this.num(p2.x)+','+this.num(p2.y)+' '+this.num(p.x)+','+this.num(p.y); return this; }
	sp(p2: GPoint, p: GPoint): GSvgPath { this.path += ' s'+this.num(p2.x)+','+this.num(p2.y)+' '+this.num(p.x)+','+this.num(p.y); return this; }

	Qp(p1: GPoint, p: GPoint): GSvgPath { this.path += ' Q'+this.num(p1.x)+','+this.num(p1.y)+' '+this.num(p.x)+','+this.num(p.y); return this; }
	qp(p1: GPoint, p: GPoint): GSvgPath { this.path += ' q'+this.num(p1.x)+','+this.num(p1.y)+' '+this.num(p.x)+','+this.num(p.y); return this; }

	Tp(p: GPoint): GSvgPath { this.path += ' T'+this.num(p.x)+','+this.num(p.y); return this; }
	tp(p: GPoint): GSvgPath { this.path += ' t'+this.num(p.x)+','+this.num(p.y); return this; }


	Ap(rx: number, ry: number, x_axis_rotation: number, large_arc_flag: boolean, sweep_flag: boolean, p: GPoint): GSvgPath
	{
		this.path += ' A' + this.num(rx) + ',' + this.num(ry) + ' ' + x_axis_rotation.toFixed(2)
			+ ' ' + (large_arc_flag ? '1' : '0') + ' ' + (sweep_flag ? '1' : '0')
			+ ' ' + this.num(p.x) + ',' + this.num(p.y);
		return this;
	}


	ap(rx: number, ry: number, x_axis_rotation: number, large_arc_flag: boolean, sweep_flag: boolean, p: GPoint): GSvgPath
	{
		this.path += ' a' + this.num(rx) + ',' + this.num(ry) + ' ' + x_axis_rotation.toFixed(2)
			+ ' ' + (large_arc_flag ? '1' : '0') + ' ' + (sweep_flag ? '1' : '0' )
			+ ' ' + this.num(p.x) + ',' + this.num(p.y);
		return this;
	}


	// Continuation (may not make sense)
	// ---------------------------------

	_(x: number, y: number): GSvgPath
	{
		this.path += ' '+this.num(x)+','+this.num(y);
		return this;
	}

	_p(p: GPoint): GSvgPath
	{
		this.path += ' '+this.num(p.x)+','+this.num(p.y);
		return this;
	}


	// Array operations (may not make sense)
	// -------------------------------------

	_v(command: string | null, points: GPoint[]): GSvgPath
	{
		if (command) {
			this.path += ' ' + command;
		}
		for (let i = 0; i < points.length; i++) {
			let p = points[i];
			this.path += ' '+this.num(p.x)+','+this.num(p.y);
		}
		return this;
	}

}

