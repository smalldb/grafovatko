/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import * as SvgIntersections from 'svg-intersections';
import GGraph from './GGraph';
import GNode from './GNode';
import GPoint from './GPoint';
import GSize from "./GSize";
import GSvg from './GSvg';
import GTransformation from './GTransformation';
import GVector from './GVector';


export default class GShape
{
	private static cacheTextMeasureCanvasContext = null;
	private static cache1emWidth = null;
	public readonly paddingEm: number = 0.75;
	public readonly marginEm: number = 2;
	public readonly minHeightEm: number = 2;
	public readonly minWidthEm: number = 2;
	public readonly strokeWidth: number = 1.5;


	/**
	 * Render node `node` using Hyperscript `h`.
	 *
	 * @param svg Hyperscript-compatible element factory/renderer.
	 * @param node Node to render.
	 * @param renderNestedGraph Nested graph renderer callback.
	 */
	public render(svg: GSvg, node: GNode, renderNestedGraph?: (graph: GGraph, transformation?: GTransformation) => any): any
	{
		let nodeClass = node.attr('class');
		let pos = node.getPosition().toRef(node.graph);
		let tooltip = node.attr('tooltip');
		return svg.g({
			'class': nodeClass ? 'node ' + nodeClass : 'node',
			'key': node.id,
			'transform': 'translate(' + pos.x + ',' + pos.y + ')',
			'opacity': node.attr('opacity'),
		}, [
			this.renderElements(svg, node),
			tooltip != "" ? svg.title([], [tooltip]) : null,
			renderNestedGraph && node.hasNestedGraph() ? renderNestedGraph(node.getNestedGraph()) : null,
		]);
	}


	/**
	 * Render elements of the shape. The render() will add the wrapping `<g>`.
	 *
	 * @see render()
	 *
	 * @param svg Hyperscript-compatible element factory/renderer.
	 * @param node Node to render.
	 */
	protected renderElements(svg: GSvg, node: GNode): any[]
	{
		return [
			this.renderLabel(svg, node),
		];
	}


	/**
	 * Render node's label.
	 *
	 * @param svg Hyperscript-compatible element factory/renderer.
	 * @param node Node to render.
	 */
	protected renderLabel(svg: GSvg, node: GNode): any
	{
		let s = node.getSize();
		return svg.text({
			'text-anchor': "middle",
			'font-size': "11pt",
			'font-family': "sans-serif",
			'fill': node.attr('color'),
			'x': 0,
			'y': node.hasNestedGraph() ? - s.height / 2 + 1.25 * this.get1em(node) : '0.33em',
		}, [node.attr('label', node.id)]);
	}


	/**
	 * Returns vector starting in sourcePoint and ending in the connector point.
	 */
	public getConnectorVector(node: GNode, sourcePoint: GPoint, connectorName?: string): GVector
	{
		let sp = sourcePoint.toRef(node.graph);
		let np = node.getPosition().toRef(node.graph);

		// Build SVG shapes in coordinates relative to node
		let line = SvgIntersections.shape("line", { x1: sp.x - np.x, y1: sp.y - np.y, x2: 0, y2: 0 });
		let shapes = this.getSvgIntersectionsShapes(node);
		let bestVector;

		// Examine intersections
		for (let shape of shapes) {
			let s = SvgIntersections.shape(shape[0], shape[1]);
			let intersections = SvgIntersections.intersect(line, s);
			for (let p of intersections.points) {
				if (p) {
					// Build connection vector
					let v = new GVector(sp, new GPoint(p.x + np.x, p.y + np.y, node.graph), node.graph);
					if (!bestVector || v.size < bestVector.size) {
						bestVector = v;
					}
				} 
			}
		}

		if (bestVector) {
			// Return best connection vector
			return bestVector;
		} else {
			// No intersection found, return a fallback vector to center of the node
			return new GVector(sourcePoint, node.getPosition(), node.graph);
		}
	}


	/**
	 * Return SVG Intersections shapes defining outline of the shape
	 *
	 * @see getConnectorVector()
	 */
	protected getSvgIntersectionsShapes(node: GNode): any[]
	{
		return [];
	}


	public getLabelSize(node: GNode): GSize
	{
		let em = this.get1em(node);
		let padding2 = 2 * this.getPadding(node);
		return new GSize(Math.max(this.getTextWidth(node), this.minWidthEm * em) + padding2,
			Math.max(em, this.minHeightEm * em) + padding2);
	}


	public getMinSize(node: GNode): GSize
	{
		return this.getLabelSize(node);
	}


	public getNestedGraphTransformation(node: GNode): GTransformation
	{
		let position = node.getPosition();
		if (node.hasNestedGraph()) {
			let bb = node.getNestedGraph().getBoundingBox();
			return new GTransformation(position.x - (bb.tlx + bb.brx) / 2, position.y - (bb.tly + bb.bry) / 2);
		} else {
			return new GTransformation(position.x, position.y);
		}
	}


	public getTextWidth(node: GNode): number
	{
		return this.getTextMetrics(node.attr('label', node.id)).width;
	}


	public getPadding(node: GNode): number
	{
		return this.paddingEm * this.get1em(node);
	}


	public getMargin(node: GNode): number
	{
		return this.marginEm * this.get1em(node);
	}


	protected getTextMetrics(text: string)
	{
		// See https://jsperf.com/measure-text-width
		if (!GShape.cacheTextMeasureCanvasContext) {
			let canvas = document.createElement('canvas');
			let docFragment = document.createDocumentFragment();
			docFragment.appendChild(canvas);
			GShape.cacheTextMeasureCanvasContext = canvas.getContext("2d");
			GShape.cacheTextMeasureCanvasContext.font = '11pt sans-serif';
		}
		return GShape.cacheTextMeasureCanvasContext.measureText(text);
	}


	protected get1em(node: GNode)
	{
		if (!GShape.cache1emWidth) {
			GShape.cache1emWidth = this.getTextMetrics('M').width;
		}
		return GShape.cache1emWidth;
	}


	protected parseSvgString(svgSource: string): Element
	{
		let parser = new DOMParser();
		return parser.parseFromString('<svg xmlns="http://www.w3.org/2000/svg">' + svgSource + '</svg>', "image/svg+xml").documentElement.children[0];
	}

}

