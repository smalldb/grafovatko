/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GBoundingBox from './GBoundingBox';
import GElement from './GElement';
import GGraph from './GGraph';
import GNode from './GNode';
import GPoint from './GPoint';


export default class GEdge extends GElement
{
	public start: GNode;
	public end: GNode;
	public points: GPoint[];
	public labelPosition: GPoint;


	constructor(graph: GGraph, id: string, attrs: any, start: GNode, end: GNode, points: GPoint[] = [])
	{
		super(graph, id, attrs);
		this.start = start;
		this.end = end;
		this.points = points;
	}


	public getAttributeDefaultValue(attrName: string): any
	{
		return this.graph.getEdgeAttributeDefaultValue(attrName);
	}


	public getBoundingBox(): GBoundingBox
	{
		let bb = new GBoundingBox(undefined, undefined, undefined, undefined, this.graph);
		let startPos = this.start.getPosition();
		let endPos = this.end.getPosition();
		if (startPos && endPos) {
			bb.addPoint(startPos);
			bb.addPoint(endPos);
			for (let p of this.points) {
				bb.addPoint(p);
			}
		}
		return bb;
	}


	public getArrowHead(): string
	{
		return this.attr('arrowHead', 'normal');
	}

	
	public getArrowTail(): string
	{
		return this.attr('arrowTail', 'none');
	}

}

