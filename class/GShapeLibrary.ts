/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GShape from './GShape';
import GShapeBpmnEvent from './GShapeBpmnEvent';
import GShapeBpmnGateway from './GShapeBpmnGateway';
import GShapeBpmnTask from './GShapeBpmnTask';
import GShapeCircle from './GShapeCircle';
import GShapeDiamond from './GShapeDiamond';
import GShapeEllipse from './GShapeEllipse';
import GShapeHexagon from './GShapeHexagon';
import GShapeLabel from './GShapeLabel';
import GShapeNestedGraph from './GShapeNestedGraph';
import GShapeNone from './GShapeNone';
import GShapeNote from './GShapeNote';
import GShapeOctagon from './GShapeOctagon';
import GShapePath from './GShapePath';
import GShapePentagon from './GShapePentagon';
import GShapePoint from './GShapePoint';
import GShapePolygon from './GShapePolygon';
import GShapeRect from './GShapeRect';
import GShapeRegularPolygon from './GShapeRegularPolygon';
import GShapeRound from './GShapeRound';
import GShapeSeptagon from './GShapeSeptagon';
import GShapeSquare from './GShapeSquare';
import GShapeSvg from './GShapeSvg';
import GShapeUmlState from './GShapeUmlState';
import GShapeUmlStateFinal from './GShapeUmlStateFinal';
import GShapeUmlStateInitial from './GShapeUmlStateInitial';


export default class GShapeLibrary
{
	private shapeCache: {[name: string]: GShape} = {};

	protected shapeList: {[name: string]: typeof GShape} = {
		'none': GShapeNone,
		'label': GShapeLabel,
		'rect': GShapeRect,
		'square': GShapeSquare,
		'ellipse': GShapeEllipse,
		'oval': GShapeEllipse,
		'circle': GShapeCircle,
		'point': GShapePoint,
		'polygon': GShapePolygon,
		'regular_polygon': GShapeRegularPolygon,
		'pentagon': GShapePentagon,
		'hexagon': GShapeHexagon,
		'septagon': GShapeSeptagon,
		'octagon': GShapeOctagon,
		'diamond': GShapeDiamond,
		'path': GShapePath,
		'round': GShapeRound,
		'nested_graph': GShapeNestedGraph,
		'svg': GShapeSvg,
		'uml.initial_state': GShapeUmlStateInitial,
		'uml.state': GShapeUmlState,
		'uml.final_state': GShapeUmlStateFinal,
		'bpmn.event': GShapeBpmnEvent,
		'bpmn.task': GShapeBpmnTask,
		'bpmn.gateway': GShapeBpmnGateway,
		'note': GShapeNote,
	};


	public getShape(name: string): GShape
	{
		if (name in this.shapeCache) {
			return this.shapeCache[name];
		} else {
			let shape = this.createShape(name);
			this.shapeCache[name] = shape;
			return shape;
		}
	}


	public listShapes(): string[]
	{
		return Object.keys(this.shapeList);
	}


	protected createShape(name: string): GShape
	{
		if (name in this.shapeList) {
			return new (this.shapeList[name])();
		} else {
			return undefined;
		}
	}

}

