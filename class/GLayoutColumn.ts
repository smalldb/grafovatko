/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GGraph from './GGraph';
import GLayout from './GLayout';
import GPoint from './GPoint';
import GSize from "./GSize";
import GSortSwimLane from './GSortSwimLane';
import GSortTopological from './GSortTopological';
import GSortAttribute from "./GSortAttribute";


export default class GLayoutColumn extends GLayout
{
	public readonly isVertical: boolean = true;

	public apply(graph: GGraph)
	{
		let spacing = this.option('spacing', 1);
		let align = this.option('align', 0);
		let edgeDistMul = this.option('edgeDistMul', 1 / 3);

		// Aliases for useful values (we don't care that some of them may not make sense)
		switch (align) {
			case 'left':   align = -1; break;
			case 'top':    align = -1; break;
			case 'center': align =  0; break;
			case 'middle': align =  0; break;
			case 'right':  align = +1; break;
			case 'bottom': align = +1; break;
		}

		// Little helpers to swap dimensions
		let width, height, x, y, createPoint;
		if (this.isVertical) {
			width = (s: GSize) => s.width;
			height = (s: GSize) => s.height;
			x = (p: GPoint) => p.x;
			y = (p: GPoint) => p.y;
			createPoint = (px: number, py: number, ref: GGraph) => new GPoint(px, py, ref);
		} else {
			width = (s: GSize) => s.height;
			height = (s: GSize) => s.width;
			x = (p: GPoint) => p.y;
			y = (p: GPoint) => p.x;
			createPoint = (px: number, py: number, ref: GGraph) => new GPoint(py, px, ref);
		}

		let yOffset = 0;
		let maxWidth = 0;

		if (align != 0) {
			for (const node of graph.nodes) {
				maxWidth = Math.max(maxWidth, width(node.getSize()));
			}
		}

		// Sort nodes to minimize edge lengths and crossings
		let sortedNodes;
		let sortAlg = this.option('sortNodes', false);
		switch (sortAlg) {
			case false:
				sortedNodes = graph.nodes;
				break;
			case true:
			case 'swimlane':
				let sls = new GSortSwimLane(graph);
				sortedNodes = sls.sort();
				break;
			case 'topological':
				let ts = new GSortTopological(graph);
				sortedNodes = ts.sort();
				break;
			case 'attr':
			case 'attribute':
				let as = new GSortAttribute(graph);
				let sortAttr = this.option('sortAttr', undefined);
				if (sortAttr === undefined) {
					throw new Error("Unspecified sort attribute (sortAttr) for attribute sort algorithm.");
				}
				sortedNodes = as.sort(sortAttr);
				break;
			default:
				throw new Error("Unknown sort algorithm: " + sortAlg);
		}

		// Place nodes
		let prevMargin;
		for (let n of sortedNodes) {
			let s = n.getSize();
			if (!s) {
				throw new Error("Node size not known: " + n.id);
			}
			let h = height(s);
			let w = width(s);
			let thisMargin = n.getMargin();
			yOffset += spacing * (prevMargin === undefined ? 0 : Math.max(prevMargin, thisMargin));
			yOffset += h / 2;
			n.setPosition(createPoint(align == 0 ? 0 : align * (maxWidth - w) / 2, yOffset, graph));
			yOffset += h / 2;
			prevMargin = thisMargin;
		}

		// Place edges
		if (this.option('arcEdges', true)) {
			for (let e of graph.edges) {
				if (e.start.graph == graph && e.end.graph == graph) {
					let endPosition = e.end.getPosition();
					let startPosition = e.start.getPosition();
					if (!startPosition) {
						throw new Error("Node not placed: " + e.start.id);
					}
					if (!endPosition) {
						throw new Error("Node not placed: " + e.end.id);
					}
					let d = Math.abs(y(endPosition) - y(startPosition));
					let h = height(e.start.getSize()) / 2 + height(e.end.getSize()) / 2 + spacing;

					if (d > h + 1 && e.points.length == 0) {
						e.points.push(createPoint((this.isVertical ? 1 : -1) * d * edgeDistMul, (y(startPosition) + y(endPosition)) / 2, graph));
					}
				}
			}
		}
	}

}

