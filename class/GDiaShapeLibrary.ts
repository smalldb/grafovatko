/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GJsonShapeLibrary from './GJsonShapeLibrary';


export default class GDiaShapeLibrary extends GJsonShapeLibrary
{
	protected jsonShapeData: any = {
    "BPMN.Activity-Looping": {
        "metadata": {
            "name": "BPMN - Activity-Looping",
            "icon": "Activity-Looping.png",
            "aspectratio": "free",
            "textbox": [
                0.25,
                0.25,
                8.75,
                5.75
            ]
        },
        "connection_points": [
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 8,
                "y": 0
            },
            {
                "x": 4.5,
                "y": 0
            },
            {
                "x": 1,
                "y": 6
            },
            {
                "x": 8,
                "y": 6
            },
            {
                "x": 4.5,
                "y": 6
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 9,
                "y": 1
            },
            {
                "x": 9,
                "y": 5
            },
            {
                "x": 9,
                "y": 3
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0z",
                    "fill": "#fff",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "path",
                {
                    "d": "M 16.8 23.2 C 14,21.8 15,17.8 17.8,17.8 C 20.6,17.8 22,21.4 19,23.4",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 16.7,
                    "y1": 23.2,
                    "x2": 16.5,
                    "y2": 21.8,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 16.9,
                    "y1": 23.2,
                    "x2": 15.3,
                    "y2": 23.2,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Ad-Hoc-Collapsed-Sub-Process": {
        "metadata": {
            "name": "BPMN - Ad-Hoc-Collapsed-Sub-Process",
            "icon": "Ad-Hoc-Collapsed-Sub-Process.png",
            "aspectratio": "free",
            "textbox": [
                0.25,
                0.25,
                8.75,
                5.75
            ]
        },
        "connection_points": [
            {
                "x": 1,
                "y": 6
            },
            {
                "x": 8,
                "y": 6
            },
            {
                "x": 4.5,
                "y": 6
            },
            {
                "x": 9,
                "y": 1
            },
            {
                "x": 9,
                "y": 5
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 8,
                "y": 0
            },
            {
                "x": 4.5,
                "y": 0
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0z",
                    "fill": "#fff",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 14.6,
                    "y": 20,
                    "width": 4,
                    "height": 4,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 14.6,
                    "y": 20,
                    "width": 4,
                    "height": 4,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 16.6,
                    "y1": 21,
                    "x2": 16.6,
                    "y2": 23,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 15.6,
                    "y1": 22,
                    "x2": 17.6,
                    "y2": 22,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "path",
                {
                    "d": "M 24 20 C 25.2,20 25.3832,19.2668 25.4,19.6 C 25.4168,19.9332 25.4,20.3 25.4,20.7 C 25.4,21.1 25.5,21.4 24,21.4 C 22.5,21.4 22.6,20.6 21.5,20.8 C 20.4,21 20,21.7 20,21.2 C 20,20.7 20,20.2 20,20 C 20,19.8 20.1,19.5 21.6,19.5 C 23.1,19.5 22.8,20 24,20z",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "path",
                {
                    "d": "M 24 20 C 25.2,20 25.3832,19.2668 25.4,19.6 C 25.4168,19.9332 25.4,20.3 25.4,20.7 C 25.4,21.1 25.5,21.4 24,21.4 C 22.5,21.4 22.6,20.6 21.5,20.8 C 20.4,21 20,21.7 20,21.2 C 20,20.7 20,20.2 20,20 C 20,19.8 20.1,19.5 21.6,19.5 C 23.1,19.5 22.8,20 24,20",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Collapsed-Sub-Process": {
        "metadata": {
            "name": "BPMN - Collapsed-Sub-Process",
            "icon": "Collapsed-Sub-Process.png",
            "aspectratio": "free",
            "textbox": [
                0.25,
                0.25,
                8.75,
                5.75
            ]
        },
        "connection_points": [
            {
                "x": 1,
                "y": 6
            },
            {
                "x": 8,
                "y": 6
            },
            {
                "x": 4.5,
                "y": 6
            },
            {
                "x": 9,
                "y": 1
            },
            {
                "x": 9,
                "y": 5
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 8,
                "y": 0
            },
            {
                "x": 4.5,
                "y": 0
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0z",
                    "fill": "#fff",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 16,
                    "y": 20,
                    "width": 4,
                    "height": 4,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 16,
                    "y": 20,
                    "width": 4,
                    "height": 4,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 18,
                    "y1": 21,
                    "x2": 18,
                    "y2": 23,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 17,
                    "y1": 22,
                    "x2": 19,
                    "y2": 22,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Compensation-Collapsed-Sub-Process": {
        "metadata": {
            "name": "BPMN - Compensation-Collapsed-Sub-Process",
            "icon": "Compensation-Collapsed-Sub-Process.png",
            "aspectratio": "free",
            "textbox": [
                0.25,
                0.25,
                8.75,
                5.75
            ]
        },
        "connection_points": [
            {
                "x": 1,
                "y": 6
            },
            {
                "x": 8,
                "y": 6
            },
            {
                "x": 4.5,
                "y": 6
            },
            {
                "x": 9,
                "y": 1
            },
            {
                "x": 9,
                "y": 5
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 8,
                "y": 0
            },
            {
                "x": 4.5,
                "y": 0
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0z",
                    "fill": "#fff",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 17.8,
                    "y": 19.9,
                    "width": 4,
                    "height": 4,
                    "fill": "#fff",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 19.8,
                    "y1": 20.9,
                    "x2": 19.8,
                    "y2": 22.9,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 18.8,
                    "y1": 21.9,
                    "x2": 20.8,
                    "y2": 21.9,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "13.8,19 11.85,21 13.8,23 ",
                    "fill": "#000000",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "16.2092,18.9716 14.2592,20.9716 16.2092,22.9716 ",
                    "fill": "#000000",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Compensation": {
        "metadata": {
            "name": "BPMN - Compensation",
            "icon": "Compensation.png",
            "aspectratio": "free",
            "textbox": [
                0.25,
                0.25,
                8.75,
                5.75
            ]
        },
        "connection_points": [
            {
                "x": 1,
                "y": 6
            },
            {
                "x": 8,
                "y": 6
            },
            {
                "x": 4.5,
                "y": 6
            },
            {
                "x": 9,
                "y": 1
            },
            {
                "x": 9,
                "y": 5
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 8,
                "y": 0
            },
            {
                "x": 4.5,
                "y": 0
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 24,
                    "x2": 32,
                    "y2": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 36,
                    "y1": 4,
                    "x2": 36,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 4,
                    "x2": 0,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 0,
                    "x2": 32,
                    "y2": 0,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "17.2,19.2 15.25,21.2 17.2,23.2 ",
                    "fill": "#000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "17.2,19.2 15.25,21.2 17.2,23.2 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "19.6092,19.1716 17.6592,21.1716 19.6092,23.1716 ",
                    "fill": "#000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "19.6092,19.1716 17.6592,21.1716 19.6092,23.1716 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "BPMN.Data-Object": {
        "metadata": {
            "name": "BPMN - Data-Object",
            "icon": "Data-Object.png",
            "aspectratio": "fixed",
            "textbox": [
                -3.5,
                4.5,
                6.5,
                10.5
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 2
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 1.5,
                "y": 4
            },
            {
                "x": 3,
                "y": 2
            },
            {
                "x": 3,
                "y": 2.5
            },
            {
                "x": 3,
                "y": 3
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "0,0 8,0 8,4 12,4 12,16 0,16 ",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "0,0 8,0 8,4 12,4 12,16 0,16 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "8,0 12,4 8,4 ",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "8,0 12,4 8,4 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.End-Event-Cancel": {
        "metadata": {
            "name": "BPMN - End-Event-Cancel",
            "icon": "End-Event-Cancel.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "4,8 16,20 4,32 8,36 20,24 32,36 36,32 24,20 36,8 32,4 20,16 8,4 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "4,8 16,20 4,32 8,36 20,24 32,36 36,32 24,20 36,8 32,4 20,16 8,4 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.End-Event-Compensation": {
        "metadata": {
            "name": "BPMN - End-Event-Compensation",
            "icon": "End-Event-Compensation.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 19.95856,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 19.95856,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 19.95856,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 19.95856,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "15.95856,4 15.95856,36 -0.0414212,20 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "15.95856,4 15.95856,36 -0.0414212,20 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "32,4 32,36 16,20 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "32,4 32,36 16,20 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.End-Event-Error": {
        "metadata": {
            "name": "BPMN - End-Event-Error",
            "icon": "End-Event-Error.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "rect",
                {
                    "x": 0,
                    "y": 0,
                    "width": 40,
                    "height": 40,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "4,36 12,16 32,32 36,4 28,24 8,8 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "4,36 12,16 32,32 36,4 28,24 8,8 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.End-Event-Link": {
        "metadata": {
            "name": "BPMN - End-Event-Link",
            "icon": "End-Event-Link.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "0,12 28,12 28,4 40,20 28,36 28,28 0,28 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "0,12 28,12 28,4 40,20 28,36 28,28 0,28 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.End-Event-Message": {
        "metadata": {
            "name": "BPMN - End-Event-Message",
            "icon": "End-Event-Message.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 4,
                    "y": 8,
                    "width": 32,
                    "height": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 4,
                    "y": 8,
                    "width": 32,
                    "height": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 8,
                    "x2": 20,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 36,
                    "y1": 8,
                    "x2": 20,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.End-Event-Multiple": {
        "metadata": {
            "name": "BPMN - End-Event-Multiple",
            "icon": "End-Event-Multiple.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "2,9.90384 14,9.90384 20,-0.0961544 26,9.90384 38,9.90384 32,19.90384 38,29.90384 26,29.90384 20,39.90384 14,29.90384 2,29.90384 8,19.90384 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "2,9.90384 14,9.90384 20,-0.0961544 26,9.90384 38,9.90384 32,19.90384 38,29.90384 26,29.90384 20,39.90384 14,29.90384 2,29.90384 8,19.90384 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "2,9.90384 14,9.90384 20,-0.0961544 26,9.90384 38,9.90384 32,19.90384 38,29.90384 26,29.90384 20,39.90384 14,29.90384 2,29.90384 8,19.90384 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 0.01,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.End-Event-Terminate": {
        "metadata": {
            "name": "BPMN - End-Event-Terminate",
            "icon": "End-Event-Terminate.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 16,
                    "ry": 16,
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 16,
                    "ry": 16,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.End-Event": {
        "metadata": {
            "name": "BPMN - End-Event",
            "icon": "End-Event.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Gateway-Complex": {
        "metadata": {
            "name": "BPMN - Gateway-Complex",
            "icon": "Gateway-Complex.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 6,
                "y": 0
            },
            {
                "x": 12,
                "y": 6
            },
            {
                "x": 6,
                "y": 12
            },
            {
                "x": 0,
                "y": 6
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 9,
                "y": 9
            },
            {
                "x": 3,
                "y": 9
            },
            {
                "x": 3,
                "y": 3
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "22,10 26,10 25.98996,19.327 32.66196,12.655 35.33396,15.327 28.6452,22.00416 38,22 38,26 28.6612,25.98816 35.3172,32.66016 32.6452,35.31616 25.999,28.66748 26,38 22,38 21.9916,28.65396 15.3332,35.31616 12.6612,32.64416 19.3172,25.98816 10,26 10,22 19.3332,22.00096 12.6612,15.33216 15.3332,12.66632 21.99796,19.327 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "22,10 26,10 25.98996,19.327 32.66196,12.655 35.33396,15.327 28.6452,22.00416 38,22 38,26 28.6612,25.98816 35.3172,32.66016 32.6452,35.31616 25.999,28.66748 26,38 22,38 21.9916,28.65396 15.3332,35.31616 12.6612,32.64416 19.3172,25.98816 10,26 10,22 19.3332,22.00096 12.6612,15.33216 15.3332,12.66632 21.99796,19.327 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Gateway-Exclusive-XOR-Data-Based": {
        "metadata": {
            "name": "BPMN - Gateway-Exclusive-XOR-Data-Based",
            "icon": "Gateway-Exclusive-XOR-Data-Based.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 6,
                "y": 0
            },
            {
                "x": 12,
                "y": 6
            },
            {
                "x": 6,
                "y": 12
            },
            {
                "x": 0,
                "y": 6
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 9,
                "y": 9
            },
            {
                "x": 3,
                "y": 9
            },
            {
                "x": 3,
                "y": 3
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "14.86172,35.92616 21.70636,24.03104 14.86172,12.07836 19.42172,12.07836 23.9914,20.0374 28.56372,12.0732 33.13852,12.07836 26.27988,24.00136 33.13724,35.92616 28.56788,35.92616 23.98912,27.98536 19.41832,35.92692 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "14.86172,35.92616 21.70636,24.03104 14.86172,12.07836 19.42172,12.07836 23.9914,20.0374 28.56372,12.0732 33.13852,12.07836 26.27988,24.00136 33.13724,35.92616 28.56788,35.92616 23.98912,27.98536 19.41832,35.92692 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Gateway-Exclusive-XOR-Event-Based": {
        "metadata": {
            "name": "BPMN - Gateway-Exclusive-XOR-Event-Based",
            "icon": "Gateway-Exclusive-XOR-Event-Based.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 6,
                "y": 0
            },
            {
                "x": 12,
                "y": 6
            },
            {
                "x": 6,
                "y": 12
            },
            {
                "x": 0,
                "y": 6
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 9,
                "y": 9
            },
            {
                "x": 3,
                "y": 9
            },
            {
                "x": 3,
                "y": 3
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 24,
                    "cy": 24,
                    "rx": 14,
                    "ry": 14,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 24,
                    "cy": 24,
                    "rx": 14,
                    "ry": 14,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 24,
                    "cy": 24,
                    "rx": 12,
                    "ry": 12,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 24,
                    "cy": 24,
                    "rx": 12,
                    "ry": 12,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "14,19 20.66668,19 24,14 27.33332,19 34,19 30.66668,24 34,29 27.33332,29 24,34 20.66668,29 14,29 17.33332,24 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "14,19 20.66668,19 24,14 27.33332,19 34,19 30.66668,24 34,29 27.33332,29 24,34 20.66668,29 14,29 17.33332,24 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "14,19 20.66668,19 24,14 27.33332,19 34,19 30.66668,24 34,29 27.33332,29 24,34 20.66668,29 14,29 17.33332,24 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 0.01,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Gateway-Inclusive-OR": {
        "metadata": {
            "name": "BPMN - Gateway-Inclusive-OR",
            "icon": "Gateway-Inclusive-OR.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 6,
                "y": 0
            },
            {
                "x": 12,
                "y": 6
            },
            {
                "x": 6,
                "y": 12
            },
            {
                "x": 0,
                "y": 6
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 9,
                "y": 9
            },
            {
                "x": 3,
                "y": 9
            },
            {
                "x": 3,
                "y": 3
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 24,
                    "cy": 24,
                    "rx": 14,
                    "ry": 14,
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 24,
                    "cy": 24,
                    "rx": 14,
                    "ry": 14,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 24,
                    "cy": 24,
                    "rx": 12,
                    "ry": 12,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 24,
                    "cy": 24,
                    "rx": 12,
                    "ry": 12,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Gateway-Parallel-AND": {
        "metadata": {
            "name": "BPMN - Gateway-Parallel-AND",
            "icon": "Gateway-Parallel-AND.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 6,
                "y": 0
            },
            {
                "x": 12,
                "y": 6
            },
            {
                "x": 6,
                "y": 12
            },
            {
                "x": 0,
                "y": 6
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 9,
                "y": 9
            },
            {
                "x": 3,
                "y": 9
            },
            {
                "x": 3,
                "y": 3
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "22,10 26,10 26,22 38,22 38,26 26,26 26,38 22,38 22,26 10,26 10,22 22,22 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "22,10 26,10 26,22 38,22 38,26 26,26 26,38 22,38 22,26 10,26 10,22 22,22 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Gateway": {
        "metadata": {
            "name": "BPMN - Gateway",
            "icon": "Gateway.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 6,
                "y": 0
            },
            {
                "x": 12,
                "y": 6
            },
            {
                "x": 6,
                "y": 12
            },
            {
                "x": 0,
                "y": 6
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 9,
                "y": 9
            },
            {
                "x": 3,
                "y": 9
            },
            {
                "x": 3,
                "y": 3
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "24,0 48,24 24,48 0,24 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Group": {
        "metadata": {
            "name": "BPMN - Group",
            "icon": "Group.png",
            "aspectratio": "free"
        },
        "connection_points": [],
        "svg": [
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke-dasharray": "1 0.4 0.2 0.4",
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Intermediate-Event-Cancel": {
        "metadata": {
            "name": "BPMN - Intermediate-Event-Cancel",
            "icon": "Intermediate-Event-Cancel.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "4,8 16,20 4,32 8,36 20,24 32,36 36,32 24,20 36,8 32,4 20,16 8,4 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "4,8 16,20 4,32 8,36 20,24 32,36 36,32 24,20 36,8 32,4 20,16 8,4 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Intermediate-Event-Compensation": {
        "metadata": {
            "name": "BPMN - Intermediate-Event-Compensation",
            "icon": "Intermediate-Event-Compensation.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 19.95856,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 19.95856,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 19.95856,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 19.95856,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "15.95856,4 15.95856,36 -0.0414212,20 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "15.95856,4 15.95856,36 -0.0414212,20 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "32,4 32,36 16,20 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "32,4 32,36 16,20 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Intermediate-Event-Error": {
        "metadata": {
            "name": "BPMN - Intermediate-Event-Error",
            "icon": "Intermediate-Event-Error.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "rect",
                {
                    "x": 0,
                    "y": 0,
                    "width": 40,
                    "height": 40,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "4,36 12,16 32,32 36,4 28,24 8,8 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "4,36 12,16 32,32 36,4 28,24 8,8 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Intermediate-Event-Link": {
        "metadata": {
            "name": "BPMN - Intermediate-Event-Link",
            "icon": "Intermediate-Event-Link.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "0,12 28,12 28,4 40,20 28,36 28,28 0,28 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "0,12 28,12 28,4 40,20 28,36 28,28 0,28 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Intermediate-Event-Message": {
        "metadata": {
            "name": "BPMN - Intermediate-Event-Message",
            "icon": "Intermediate-Event-Message.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 4,
                    "y": 8,
                    "width": 32,
                    "height": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 4,
                    "y": 8,
                    "width": 32,
                    "height": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 8,
                    "x2": 20,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 36,
                    "y1": 8,
                    "x2": 20,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Intermediate-Event-Multiple": {
        "metadata": {
            "name": "BPMN - Intermediate-Event-Multiple",
            "icon": "Intermediate-Event-Multiple.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "2,9.90384 14,9.90384 20,-0.0961544 26,9.90384 38,9.90384 32,19.90384 38,29.90384 26,29.90384 20,39.90384 14,29.90384 2,29.90384 8,19.90384 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "2,9.90384 14,9.90384 20,-0.0961544 26,9.90384 38,9.90384 32,19.90384 38,29.90384 26,29.90384 20,39.90384 14,29.90384 2,29.90384 8,19.90384 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "2,9.90384 14,9.90384 20,-0.0961544 26,9.90384 38,9.90384 32,19.90384 38,29.90384 26,29.90384 20,39.90384 14,29.90384 2,29.90384 8,19.90384 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 0.01,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Intermediate-Event-Rule": {
        "metadata": {
            "name": "BPMN - Intermediate-Event-Rule",
            "icon": "Intermediate-Event-Rule.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 8,
                    "y": 4,
                    "width": 24,
                    "height": 32,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 8,
                    "y": 4,
                    "width": 24,
                    "height": 32,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 12,
                    "y1": 8,
                    "x2": 28,
                    "y2": 8,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 12,
                    "y1": 16,
                    "x2": 28,
                    "y2": 16,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 12,
                    "y1": 24,
                    "x2": 28,
                    "y2": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 12,
                    "y1": 32,
                    "x2": 28,
                    "y2": 32,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Intermediate-Event-Timer": {
        "metadata": {
            "name": "BPMN - Intermediate-Event-Timer",
            "icon": "Intermediate-Event-Timer.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 16,
                    "ry": 16,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 16,
                    "ry": 16,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 20,
                    "y1": 4,
                    "x2": 20,
                    "y2": 8,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 20,
                    "y1": 32,
                    "x2": 20,
                    "y2": 36,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 20,
                    "x2": 8,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 32,
                    "y1": 20,
                    "x2": 36,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 14,
                    "y1": 30,
                    "x2": 11.7,
                    "y2": 33.7,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 10,
                    "y1": 26,
                    "x2": 6.3,
                    "y2": 28.3,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 10,
                    "y1": 14,
                    "x2": 6.35,
                    "y2": 11.65,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 14,
                    "y1": 10,
                    "x2": 11.75,
                    "y2": 6.35,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 26,
                    "y1": 10,
                    "x2": 28.26884,
                    "y2": 6.25,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 30,
                    "y1": 14,
                    "x2": 33.76884,
                    "y2": 11.75,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 30,
                    "y1": 26,
                    "x2": 33.76884,
                    "y2": 28.28944,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 26,
                    "y1": 30,
                    "x2": 28.26884,
                    "y2": 33.78944,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 20,
                    "y1": 20,
                    "x2": 28,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 20,
                    "y1": 20,
                    "x2": 22.46884,
                    "y2": 7.4748,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Intermediate-Event": {
        "metadata": {
            "name": "BPMN - Intermediate-Event",
            "icon": "Intermediate-Event.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 24,
                    "ry": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Looping-Collapsed-Sub-Process": {
        "metadata": {
            "name": "BPMN - Looping-Collapsed-Sub-Process",
            "icon": "Looping-Collapsed-Sub-Process.png",
            "aspectratio": "free",
            "textbox": [
                0.25,
                0.25,
                8.75,
                5.75
            ]
        },
        "connection_points": [
            {
                "x": 1,
                "y": 6
            },
            {
                "x": 8,
                "y": 6
            },
            {
                "x": 4.5,
                "y": 6
            },
            {
                "x": 9,
                "y": 1
            },
            {
                "x": 9,
                "y": 5
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 8,
                "y": 0
            },
            {
                "x": 4.5,
                "y": 0
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0z",
                    "fill": "#fff",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 20,
                    "y": 20,
                    "width": 4,
                    "height": 4,
                    "fill": "#fff",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 22,
                    "y1": 21,
                    "x2": 22,
                    "y2": 23,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 21,
                    "y1": 22,
                    "x2": 23,
                    "y2": 22,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "path",
                {
                    "d": "M 14.8 23 C 12,21.6 13,17.6 15.8,17.6 C 18.6,17.6 20,21.2 17,23.2",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 14.7,
                    "y1": 23,
                    "x2": 14.5,
                    "y2": 21.6,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 14.9,
                    "y1": 23,
                    "x2": 13.3,
                    "y2": 23,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Multiple-Instance-Collapsed-Sub-Process": {
        "metadata": {
            "name": "BPMN - Multiple-Instance-Collapsed-Sub-Process",
            "icon": "Multiple-Instance-Collapsed-Sub-Process.png",
            "aspectratio": "free",
            "textbox": [
                0.25,
                0.25,
                8.75,
                5.75
            ]
        },
        "connection_points": [
            {
                "x": 1,
                "y": 6
            },
            {
                "x": 8,
                "y": 6
            },
            {
                "x": 4.5,
                "y": 6
            },
            {
                "x": 9,
                "y": 1
            },
            {
                "x": 9,
                "y": 5
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 8,
                "y": 0
            },
            {
                "x": 4.5,
                "y": 0
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 24,
                    "x2": 32,
                    "y2": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 36,
                    "y1": 4,
                    "x2": 36,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 4,
                    "x2": 0,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 0,
                    "x2": 32,
                    "y2": 0,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 19,
                    "y": 20,
                    "width": 4,
                    "height": 4,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 19,
                    "y": 20,
                    "width": 4,
                    "height": 4,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 21,
                    "y1": 21,
                    "x2": 21,
                    "y2": 23,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 20,
                    "y1": 22,
                    "x2": 22,
                    "y2": 22,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 14,
                    "y": 19.2,
                    "width": 0.9,
                    "height": 3.8,
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 14,
                    "y": 19.2,
                    "width": 0.9,
                    "height": 3.8,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 16,
                    "y": 19.18,
                    "width": 0.93,
                    "height": 3.8,
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 16,
                    "y": 19.18,
                    "width": 0.93,
                    "height": 3.8,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Multiple-Instance-Task": {
        "metadata": {
            "name": "BPMN - Multiple-Instance-Task",
            "icon": "Multiple-Instance-Task.png",
            "aspectratio": "free",
            "textbox": [
                0.25,
                0.25,
                8.75,
                5.75
            ]
        },
        "connection_points": [
            {
                "x": 1,
                "y": 6
            },
            {
                "x": 8,
                "y": 6
            },
            {
                "x": 4.5,
                "y": 6
            },
            {
                "x": 9,
                "y": 1
            },
            {
                "x": 9,
                "y": 5
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 8,
                "y": 0
            },
            {
                "x": 4.5,
                "y": 0
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0z",
                    "fill": "#fff",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 16.6,
                    "y": 19.02,
                    "width": 0.9,
                    "height": 3.8,
                    "fill": "#000000",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 18.6,
                    "y": 19,
                    "width": 0.93,
                    "height": 3.8,
                    "fill": "#000000",
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Start-Event-Link": {
        "metadata": {
            "name": "BPMN - Start-Event-Link",
            "icon": "Start-Event-Link.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "0,12 28,12 28,4 40,20 28,36 28,28 0,28 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "0,12 28,12 28,4 40,20 28,36 28,28 0,28 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Start-Event-Message": {
        "metadata": {
            "name": "BPMN - Start-Event-Message",
            "icon": "Start-Event-Message.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 4,
                    "y": 8,
                    "width": 32,
                    "height": 24,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 4,
                    "y": 8,
                    "width": 32,
                    "height": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 8,
                    "x2": 20,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 36,
                    "y1": 8,
                    "x2": 20,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Start-Event-Multiple": {
        "metadata": {
            "name": "BPMN - Start-Event-Multiple",
            "icon": "Start-Event-Multiple.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "2,9.90384 14,9.90384 20,-0.0961544 26,9.90384 38,9.90384 32,19.90384 38,29.90384 26,29.90384 20,39.90384 14,29.90384 2,29.90384 8,19.90384 ",
                    "fill": "#000000",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "2,9.90384 14,9.90384 20,-0.0961544 26,9.90384 38,9.90384 32,19.90384 38,29.90384 26,29.90384 20,39.90384 14,29.90384 2,29.90384 8,19.90384 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "2,9.90384 14,9.90384 20,-0.0961544 26,9.90384 38,9.90384 32,19.90384 38,29.90384 26,29.90384 20,39.90384 14,29.90384 2,29.90384 8,19.90384 ",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 0.01,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Start-Event-Rule": {
        "metadata": {
            "name": "BPMN - Start-Event-Rule",
            "icon": "Start-Event-Rule.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 8,
                    "y": 4,
                    "width": 24,
                    "height": 32,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "rect",
                {
                    "x": 8,
                    "y": 4,
                    "width": 24,
                    "height": 32,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 12,
                    "y1": 8,
                    "x2": 28,
                    "y2": 8,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 12,
                    "y1": 16,
                    "x2": 28,
                    "y2": 16,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 12,
                    "y1": 24,
                    "x2": 28,
                    "y2": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 12,
                    "y1": 32,
                    "x2": 28,
                    "y2": 32,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Start-Event-Timer": {
        "metadata": {
            "name": "BPMN - Start-Event-Timer",
            "icon": "Start-Event-Timer.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#ffffff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 16,
                    "ry": 16,
                    "fill": "#ffffff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 16,
                    "ry": 16,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 20,
                    "y1": 4,
                    "x2": 20,
                    "y2": 8,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 20,
                    "y1": 32,
                    "x2": 20,
                    "y2": 36,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 20,
                    "x2": 8,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 32,
                    "y1": 20,
                    "x2": 36,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 14,
                    "y1": 30,
                    "x2": 11.7,
                    "y2": 33.7,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 10,
                    "y1": 26,
                    "x2": 6.3,
                    "y2": 28.3,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 10,
                    "y1": 14,
                    "x2": 6.35,
                    "y2": 11.65,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 14,
                    "y1": 10,
                    "x2": 11.75,
                    "y2": 6.35,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 26,
                    "y1": 10,
                    "x2": 28.26884,
                    "y2": 6.25,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 30,
                    "y1": 14,
                    "x2": 33.76884,
                    "y2": 11.75,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 30,
                    "y1": 26,
                    "x2": 33.76884,
                    "y2": 28.28944,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 26,
                    "y1": 30,
                    "x2": 28.26884,
                    "y2": 33.78944,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 20,
                    "y1": 20,
                    "x2": 28,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 20,
                    "y1": 20,
                    "x2": 22.46884,
                    "y2": 7.4748,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Start-Event": {
        "metadata": {
            "name": "BPMN - Start-Event",
            "icon": "Start-Event.png",
            "aspectratio": "fixed"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 5,
                "y": -2
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": 12,
                "y": 5
            }
        ],
        "svg": [
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "ellipse",
                {
                    "cx": 20,
                    "cy": 20,
                    "rx": 28,
                    "ry": 28,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Task": {
        "metadata": {
            "name": "BPMN - Task",
            "icon": "Task.png",
            "aspectratio": "free",
            "textbox": [
                0.25,
                0.25,
                8.75,
                5.75
            ]
        },
        "connection_points": [
            {
                "x": 1,
                "y": 6
            },
            {
                "x": 8,
                "y": 6
            },
            {
                "x": 4.5,
                "y": 6
            },
            {
                "x": 9,
                "y": 1
            },
            {
                "x": 9,
                "y": 5
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 8,
                "y": 0
            },
            {
                "x": 4.5,
                "y": 0
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 24,
                    "x2": 32,
                    "y2": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 36,
                    "y1": 4,
                    "x2": 36,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 4,
                    "x2": 0,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 0,
                    "x2": 32,
                    "y2": 0,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Text-Annotation": {
        "metadata": {
            "name": "BPMN - Text-Annotation",
            "icon": "Text-Annotation.png",
            "aspectratio": "fixed",
            "textbox": [
                0,
                0,
                6,
                3
            ]
        },
        "connection_points": [
            {
                "x": 6,
                "y": 1.5
            },
            {
                "x": 0,
                "y": 1.5
            }
        ],
        "svg": [
            [
                "line",
                {
                    "x1": 24,
                    "y1": 0,
                    "x2": 24,
                    "y2": 12,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#ffffff"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 0,
                    "x2": 0,
                    "y2": 12,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 12,
                    "x2": 6,
                    "y2": 12,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 0,
                    "x2": 6,
                    "y2": 0,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "BPMN.Transaction": {
        "metadata": {
            "name": "BPMN - Transaction",
            "icon": "Transaction.png",
            "aspectratio": "free"
        },
        "connection_points": [
            {
                "x": 1,
                "y": 6
            },
            {
                "x": 8,
                "y": 6
            },
            {
                "x": 4.5,
                "y": 6
            },
            {
                "x": 9,
                "y": 1
            },
            {
                "x": 9,
                "y": 5
            },
            {
                "x": 9,
                "y": 3
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 8,
                "y": 0
            },
            {
                "x": 4.5,
                "y": 0
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "path",
                {
                    "d": "M 4 0 C 8,0 28,0 32,0 C 36,0 36,0 36,4 C 36,8 36,16 36,20 C 36,24 36,24 32,24 C 28,24 8,24 4,24 C 0,24 0,24 0,20 C 0,16 0,8 0,4 C 0,0 0,0 4,0",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "path",
                {
                    "d": "M 4.7 0.7 C 8.7,0.7 27.1004,0.7 31.1004,0.7 C 35.1004,0.7 35.1004,0.7 35.1004,4.7 C 35.1004,8.7 35.1004,15.1 35.1004,19.1 C 35.1004,23.1 35.1004,23.1 31.1004,23.1 C 27.1004,23.1 8.8748,23.1 4.8748,23.1 C 0.874788,23.1 0.874788,23.1 0.874788,19.1 C 0.874788,15.1 0.9,8.9 0.9,4.9 C 0.9,0.9 0.7,0.7 4.7,0.7z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "path",
                {
                    "d": "M 4.7 0.7 C 8.7,0.7 27.1004,0.7 31.1004,0.7 C 35.1004,0.7 35.1004,0.7 35.1004,4.7 C 35.1004,8.7 35.1004,15.1 35.1004,19.1 C 35.1004,23.1 35.1004,23.1 31.1004,23.1 C 27.1004,23.1 8.8748,23.1 4.8748,23.1 C 0.874788,23.1 0.874788,23.1 0.874788,19.1 C 0.874788,15.1 0.9,8.9 0.9,4.9 C 0.9,0.9 0.7,0.7 4.7,0.7",
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 24,
                    "x2": 32,
                    "y2": 24,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 36,
                    "y1": 4,
                    "x2": 36,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 4,
                    "x2": 0,
                    "y2": 20,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 0,
                    "x2": 32,
                    "y2": 0,
                    "fill": "none",
                    "fill-opacity": 0,
                    "stroke-width": 1,
                    "stroke": "#000000"
                },
                []
            ]
        ]
    },
    "Flowchart.collate": {
        "metadata": {
            "name": "Flowchart - Collate",
            "icon": "collate.png"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 2.5
            },
            {
                "x": 5,
                "y": 5
            },
            {
                "x": 2.5,
                "y": 7.5
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 2.5
            },
            {
                "x": 7.5,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "0,0 40,0 20,20",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "polygon",
                {
                    "points": "0,40 40,40 20,20",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.datasource": {
        "metadata": {
            "name": "Flowchart - Data Source",
            "icon": "datasource.png",
            "textbox": [
                0.5,
                0.5,
                10,
                10
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0,
                "y": 2.5
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 7.5
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 10,
                "y": 2.5
            },
            {
                "x": 10,
                "y": 5
            },
            {
                "x": 10,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "line",
                {
                    "x1": 0,
                    "y1": 0,
                    "x2": 0,
                    "y2": 40,
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 0,
                    "x2": 40,
                    "y2": 0,
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 40,
                    "x2": 40,
                    "y2": 40,
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.delay": {
        "metadata": {
            "name": "Flowchart - Delay",
            "icon": "delay.png",
            "textbox": [
                0,
                0,
                4,
                4
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 2,
                "y": 0
            },
            {
                "x": 3,
                "y": 0
            },
            {
                "x": 4,
                "y": 0
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 0,
                "y": 2
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 0,
                "y": 4
            },
            {
                "x": 5.4142,
                "y": 0.5858
            },
            {
                "x": 6,
                "y": 2
            },
            {
                "x": 5.4142,
                "y": 3.4142
            },
            {
                "x": 4,
                "y": 4
            },
            {
                "x": 1,
                "y": 4
            },
            {
                "x": 2,
                "y": 4
            },
            {
                "x": 3,
                "y": 4
            },
            {
                "x": 3,
                "y": 2
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "m 0,0 h 16 c 4.41828,0 8,3.58172 8,8 s -3.58172,8 -8,8                  h -16 v -16 z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.display": {
        "metadata": {
            "name": "Flowchart - Display",
            "icon": "display.png",
            "textbox": [
                0,
                0,
                10,
                10
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": -1,
                "y": 2.5
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": -1,
                "y": 7.5
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 11.5625,
                "y": 2.5
            },
            {
                "x": 12,
                "y": 5
            },
            {
                "x": 11.5625,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 7,
                "y": 5
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 0,0 h 40 c 6,8 8,12 8,20 s -2,12 -8,20                  h -40 l -8,-20 l 8,-20 z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.document": {
        "metadata": {
            "name": "Flowchart - Document",
            "icon": "document.png",
            "textbox": [
                0,
                0,
                10,
                5
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0,
                "y": 1.5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 0,
                "y": 4.5
            },
            {
                "x": 0,
                "y": 6
            },
            {
                "x": 10,
                "y": 1.5
            },
            {
                "x": 10,
                "y": 3
            },
            {
                "x": 10,
                "y": 4.5
            },
            {
                "x": 10,
                "y": 6
            },
            {
                "x": 2.5,
                "y": 6.75
            },
            {
                "x": 5,
                "y": 6
            },
            {
                "x": 7.5,
                "y": 5.25
            },
            {
                "x": 5,
                "y": 3.5
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 0,0 h 40 v 24 c -8,-4 -12,-4 -20,0 s -12,4 -20,0 v -24 z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.extract": {
        "metadata": {
            "name": "Flowchart - Extract",
            "icon": "extract.png",
            "textbox": [
                2.5,
                5,
                7.5,
                10
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 1.25,
                "y": 7.5
            },
            {
                "x": 2.5,
                "y": 5
            },
            {
                "x": 3.75,
                "y": 2.5
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 8.75,
                "y": 7.5
            },
            {
                "x": 7.5,
                "y": 5
            },
            {
                "x": 6.25,
                "y": 2.5
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "0,40 40,40 20,0",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.intstorage": {
        "metadata": {
            "name": "Flowchart - Internal Storage",
            "icon": "intstorage.png",
            "textbox": [
                1,
                1,
                10,
                10
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0,
                "y": 2.5
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 7.5
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 10,
                "y": 2.5
            },
            {
                "x": 10,
                "y": 5
            },
            {
                "x": 10,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 7,
                "y": 5
            }
        ],
        "svg": [
            [
                "rect",
                {
                    "x": 0,
                    "y": 0,
                    "width": 40,
                    "height": 40,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 0,
                    "x2": 4,
                    "y2": 40,
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 4,
                    "x2": 40,
                    "y2": 4,
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.magdisk": {
        "metadata": {
            "name": "Flowchart - Magnetic Disk",
            "icon": "magdisk.png",
            "textbox": [
                0,
                4,
                10,
                10
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 2
            },
            {
                "x": 2.5,
                "y": 0.4375
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0.4375
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0,
                "y": 2.5
            },
            {
                "x": 0,
                "y": 7.5
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 10,
                "y": 2.5
            },
            {
                "x": 10,
                "y": 5
            },
            {
                "x": 10,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 11.5625
            },
            {
                "x": 5,
                "y": 12
            },
            {
                "x": 7.5,
                "y": 11.5625
            },
            {
                "x": 5,
                "y": 6
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 0,8 c 8,-6 12,-8 20,-8 s 12,2 20,8 v 32                  c -8,6 -12,8 -20,8 s -12,-2 -20,-8 v -32 z                  M 0,8 c 8,6 12,8 20,8 s 12,-2 20,-8",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.magdrum": {
        "metadata": {
            "name": "Flowchart - Magnetic Drum",
            "icon": "magdrum.png",
            "textbox": [
                0,
                0,
                8,
                10
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": -1.5625,
                "y": 2.5
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": -1.5625,
                "y": 7.5
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 11.5625,
                "y": 2.5
            },
            {
                "x": 12,
                "y": 5
            },
            {
                "x": 11.5625,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 7,
                "y": 5
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 0,0 h 40 c 6,8 8,12 8,20 s -2,12 -8,20                  h -40 c -6,-8 -8,-12, -8,-20 s 2,-12 8,-20 z                  M 40,0 c -6,8 -8,12 -8,20 s 2,12 8,20",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.magtape": {
        "metadata": {
            "name": "Flowchart - Magnetic Tape",
            "icon": "magtape.png",
            "textbox": [
                1.4645,
                1.4645,
                8.5355,
                8.5355
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 1.4645,
                "y": 1.4645
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 8.5355,
                "y": 1.4645
            },
            {
                "x": 10,
                "y": 5
            },
            {
                "x": 8.5355,
                "y": 8.5355
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 1.4645,
                "y": 8.5355
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 5,
                "y": 6
            }
        ],
        "svg": [
            [
                "circle",
                {
                    "cx": 20,
                    "cy": 20,
                    "r": 20,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 20,
                    "y1": 40,
                    "x2": 40,
                    "y2": 40,
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.manualinput": {
        "metadata": {
            "name": "Flowchart - Manual Input",
            "icon": "manualinput.png",
            "textbox": [
                0,
                4,
                10,
                10
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 4
            },
            {
                "x": 2.5,
                "y": 3
            },
            {
                "x": 5,
                "y": 2
            },
            {
                "x": 7.5,
                "y": 1
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0,
                "y": 5.5
            },
            {
                "x": 0,
                "y": 7
            },
            {
                "x": 0,
                "y": 8.5
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 10,
                "y": 2.5
            },
            {
                "x": 10,
                "y": 5
            },
            {
                "x": 10,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 0,16 l 40,-16 v 40 h -40 v -24 z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.manualop": {
        "metadata": {
            "name": "Flowchart - Manual Operation",
            "icon": "manualop.png",
            "textbox": [
                2,
                0,
                8,
                10
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0.5,
                "y": 2.5
            },
            {
                "x": 1,
                "y": 5
            },
            {
                "x": 1.5,
                "y": 7.5
            },
            {
                "x": 2,
                "y": 10
            },
            {
                "x": 9.5,
                "y": 2.5
            },
            {
                "x": 9,
                "y": 5
            },
            {
                "x": 8.5,
                "y": 7.5
            },
            {
                "x": 8,
                "y": 10
            },
            {
                "x": 3.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 6.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 0,0 h 40 l -8,40 h -24 l -8,-40 z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.merge": {
        "metadata": {
            "name": "Flowchart - Merge",
            "icon": "merge.png",
            "textbox": [
                2.5,
                0,
                7.5,
                5
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 1.25,
                "y": 2.5
            },
            {
                "x": 2.5,
                "y": 5
            },
            {
                "x": 3.75,
                "y": 7.5
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 8.75,
                "y": 2.5
            },
            {
                "x": 7.5,
                "y": 5
            },
            {
                "x": 6.25,
                "y": 7.5
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "0,0 40,0 20,40",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.offlinestore": {
        "metadata": {
            "name": "Flowchart - Offline Storage",
            "icon": "offlinestore.png",
            "textbox": [
                2.5,
                0,
                7.5,
                5
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 1.25,
                "y": 2.5
            },
            {
                "x": 2.5,
                "y": 5
            },
            {
                "x": 3.75,
                "y": 7.5
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 8.75,
                "y": 2.5
            },
            {
                "x": 7.5,
                "y": 5
            },
            {
                "x": 6.25,
                "y": 7.5
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "0,0 40,0 20,40",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 16,
                    "y1": 32,
                    "x2": 24,
                    "y2": 32,
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.offpageconn": {
        "metadata": {
            "name": "Flowchart - Off Page Connector",
            "icon": "offpageconn.png",
            "textbox": [
                0,
                0,
                10,
                8
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0,
                "y": 2
            },
            {
                "x": 0,
                "y": 4
            },
            {
                "x": 0,
                "y": 6
            },
            {
                "x": 0,
                "y": 8
            },
            {
                "x": 10,
                "y": 2
            },
            {
                "x": 10,
                "y": 4
            },
            {
                "x": 10,
                "y": 6
            },
            {
                "x": 10,
                "y": 8
            },
            {
                "x": 2.5,
                "y": 9
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 9
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 0,0 h 40 v 32 l -20,8 l -20,-8 v -32 z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.or": {
        "metadata": {
            "name": "Flowchart - Or",
            "icon": "or.png"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 1.4645,
                "y": 1.4645
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 8.5355,
                "y": 1.4645
            },
            {
                "x": 10,
                "y": 5
            },
            {
                "x": 8.5355,
                "y": 8.5355
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 1.4645,
                "y": 8.5355
            },
            {
                "x": 5,
                "y": 6
            }
        ],
        "svg": [
            [
                "circle",
                {
                    "cx": 20,
                    "cy": 20,
                    "r": 20,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 20,
                    "y1": 0,
                    "x2": 20,
                    "y2": 40,
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 20,
                    "x2": 40,
                    "y2": 20,
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.predefdproc": {
        "metadata": {
            "name": "Flowchart - Predefined Process",
            "icon": "predefdproc.png",
            "textbox": [
                1,
                0,
                9,
                10
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0,
                "y": 2.5
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 7.5
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 10,
                "y": 2.5
            },
            {
                "x": 10,
                "y": 5
            },
            {
                "x": 10,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "rect",
                {
                    "x": 0,
                    "y": 0,
                    "width": 40,
                    "height": 40,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 4,
                    "y1": 0,
                    "x2": 4,
                    "y2": 40,
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 36,
                    "y1": 0,
                    "x2": 36,
                    "y2": 40,
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.preparation": {
        "metadata": {
            "name": "Flowchart - Preparation",
            "icon": "preparation.png",
            "textbox": [
                0,
                0,
                10,
                10
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": -1,
                "y": 2.5
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": -1,
                "y": 7.5
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 11,
                "y": 2.5
            },
            {
                "x": 12,
                "y": 5
            },
            {
                "x": 11,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 7,
                "y": 5
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 0,0 h 40 l 8,20 l -8,20 h -40 l -8,-20 l 8,-20 z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.punchedcard": {
        "metadata": {
            "name": "Flowchart - Punched Card",
            "icon": "punchedcard.png",
            "textbox": [
                0.5,
                0.5,
                10,
                10
            ]
        },
        "connection_points": [
            {
                "x": 1,
                "y": 0
            },
            {
                "x": 0,
                "y": 1
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0,
                "y": 2.5
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 0,
                "y": 7.5
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 10,
                "y": 2.5
            },
            {
                "x": 10,
                "y": 5
            },
            {
                "x": 10,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "4,0 40,0 40,40 0,40 0,4",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.punchedtape": {
        "metadata": {
            "name": "Flowchart - Punched Tape",
            "icon": "punchedtape.png",
            "textbox": [
                0,
                1,
                10,
                5
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0.75
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": -0.75
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0,
                "y": 1.5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 0,
                "y": 4.5
            },
            {
                "x": 0,
                "y": 6
            },
            {
                "x": 10,
                "y": 1.5
            },
            {
                "x": 10,
                "y": 3
            },
            {
                "x": 10,
                "y": 4.5
            },
            {
                "x": 10,
                "y": 6
            },
            {
                "x": 2.5,
                "y": 6.75
            },
            {
                "x": 5,
                "y": 6
            },
            {
                "x": 7.5,
                "y": 5.25
            },
            {
                "x": 5,
                "y": 4
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 0,0 c 8,4 12,4 20,0 s 12,-4 20,0 v 24                  c -8,-4 -12,-4 -20,0 s -12,4 -20,0 v -24 z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.sort": {
        "metadata": {
            "name": "Flowchart - Sort",
            "icon": "sort.png"
        },
        "connection_points": [
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 3.75,
                "y": 1.25
            },
            {
                "x": 2.5,
                "y": 2.5
            },
            {
                "x": 1.25,
                "y": 3.75
            },
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 1.25,
                "y": 6.25
            },
            {
                "x": 2.5,
                "y": 7.5
            },
            {
                "x": 3.75,
                "y": 8.75
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 6.25,
                "y": 1.25
            },
            {
                "x": 7.5,
                "y": 2.5
            },
            {
                "x": 8.75,
                "y": 3.75
            },
            {
                "x": 10,
                "y": 5
            },
            {
                "x": 8.75,
                "y": 6.25
            },
            {
                "x": 7.5,
                "y": 7.5
            },
            {
                "x": 6.25,
                "y": 8.75
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "polygon",
                {
                    "points": "20,0 40,20 20,40 0,20",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 0,
                    "y1": 20,
                    "x2": 40,
                    "y2": 20,
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.sumjunction": {
        "metadata": {
            "name": "Flowchart - Summing Junction",
            "icon": "sumjunction.png"
        },
        "connection_points": [
            {
                "x": 0,
                "y": 5
            },
            {
                "x": 1.4645,
                "y": 1.4645
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 8.5355,
                "y": 1.4645
            },
            {
                "x": 10,
                "y": 5
            },
            {
                "x": 8.5355,
                "y": 8.5355
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 1.4645,
                "y": 8.5355
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "circle",
                {
                    "cx": 20,
                    "cy": 20,
                    "r": 20,
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 5.858,
                    "y1": 5.858,
                    "x2": 34.142,
                    "y2": 34.142,
                    "stroke": "#000"
                },
                []
            ],
            [
                "line",
                {
                    "x1": 5.858,
                    "y1": 34.142,
                    "x2": 34.142,
                    "y2": 5.858,
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.terminal": {
        "metadata": {
            "name": "Flowchart - Terminal",
            "icon": "terminal.png",
            "textbox": [
                2,
                0,
                10,
                4
            ]
        },
        "connection_points": [
            {
                "x": 2,
                "y": 0
            },
            {
                "x": 4,
                "y": 0
            },
            {
                "x": 6,
                "y": 0
            },
            {
                "x": 8,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0.5858,
                "y": 0.5858
            },
            {
                "x": 0,
                "y": 2
            },
            {
                "x": 0.5858,
                "y": 3.4142
            },
            {
                "x": 2,
                "y": 4
            },
            {
                "x": 11.4142,
                "y": 0.5858
            },
            {
                "x": 12,
                "y": 2
            },
            {
                "x": 11.4142,
                "y": 3.4142
            },
            {
                "x": 10,
                "y": 4
            },
            {
                "x": 4,
                "y": 4
            },
            {
                "x": 6,
                "y": 4
            },
            {
                "x": 8,
                "y": 4
            },
            {
                "x": 6,
                "y": 2
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 8,0 h 32 c 4.41828,0 8,3.58172 8,8 s -3.58172,8 -8,8                  h -32 c -4.41828,0 -8,-3.58172 -8,-8 s 3.58172,-8 8,-8 z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.transaction": {
        "metadata": {
            "name": "Flowchart - Transaction File",
            "icon": "transaction.png",
            "textbox": [
                0,
                0,
                8,
                10
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": -1.5625,
                "y": 2.5
            },
            {
                "x": -2,
                "y": 5
            },
            {
                "x": -1.5625,
                "y": 7.5
            },
            {
                "x": 0,
                "y": 10
            },
            {
                "x": 8.4375,
                "y": 2.5
            },
            {
                "x": 8,
                "y": 5
            },
            {
                "x": 8.4375,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 10
            },
            {
                "x": 5,
                "y": 10
            },
            {
                "x": 7.5,
                "y": 10
            },
            {
                "x": 7,
                "y": 5
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 0,0 h 40 c -6,8 -8,12 -8,20 s 2,12 8,20                  h -40 c -6,-8 -8,-12, -8,-20 s 2,-12 8,-20 z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    },
    "Flowchart.transmittape": {
        "metadata": {
            "name": "Flowchart - Transmittal Tape",
            "icon": "transmittape.png",
            "textbox": [
                0,
                0,
                10,
                6
            ]
        },
        "connection_points": [
            {
                "x": 0,
                "y": 0
            },
            {
                "x": 2.5,
                "y": 0
            },
            {
                "x": 5,
                "y": 0
            },
            {
                "x": 7.5,
                "y": 0
            },
            {
                "x": 10,
                "y": 0
            },
            {
                "x": 0,
                "y": 1.5
            },
            {
                "x": 0,
                "y": 3
            },
            {
                "x": 0,
                "y": 4.5
            },
            {
                "x": 0,
                "y": 6
            },
            {
                "x": 10,
                "y": 2.5
            },
            {
                "x": 10,
                "y": 5
            },
            {
                "x": 10,
                "y": 7.5
            },
            {
                "x": 10,
                "y": 10
            },
            {
                "x": 2.5,
                "y": 7.5625
            },
            {
                "x": 5,
                "y": 8
            },
            {
                "x": 7.5,
                "y": 8.4375
            },
            {
                "x": 5,
                "y": 5
            }
        ],
        "svg": [
            [
                "path",
                {
                    "d": "M 0,0 h 40 v 40 c -8,-6 -12,-8 -20,-8 s -12,-2 -20,-8 v -24 z",
                    "fill": "#fff",
                    "stroke": "#000"
                },
                []
            ]
        ]
    }
};
}

// vim:filetype=typescript:

