/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GGraph from './GGraph';
import GNode from './GNode';
import GEdge from './GEdge';

export default class GSortAttribute
{
	private graph: GGraph;

	public constructor(graph: GGraph)
	{
		this.graph = graph;
	}


	public sort(attributeName: string = 'id')
	{
		if (this.graph.nodes.length == 0) {
			return this.graph.nodes;
		}

		let nodes = this.graph.nodes;

		nodes.sort((na: GNode, nb: GNode) => {
			let aa = na.attr(attributeName);
			let ab = nb.attr(attributeName);
			return aa == ab ? 0 : (aa < ab ? -1 : 1);
		});

		return nodes;
	}

}

