/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GBoundingBox from './GBoundingBox';
import GGraph from './GGraph';
import GTransformation from './GTransformation';


export default class GPoint
{
	public readonly x: number;
	public readonly y: number;
	public readonly ref: GGraph;

	
	constructor(x, y, ref: GGraph)
	{
		this.x = x;
		this.y = y;
		this.ref = ref;

		if (!this.ref) {
			throw new Error("Missing 'ref' argument!");
		}
	}


	public toRef(destRef: GGraph): GPoint
	{
		if (this.ref === destRef || destRef === undefined) {
			return this;
		} else if (this.ref === undefined) {
			throw new Error("Can't convert to another reference. - Point's reference is not defined.");
		} else {
			return this.transform(this.ref.getAbsTransformation().diff(destRef.getAbsTransformation()), destRef);
		}
	}


	public transform(tr: GTransformation, ref?: GGraph): GPoint
	{
		return new GPoint(this.x + tr.x, this.y + tr.y, ref || this.ref);
	}


	public getBoundingBox(): GBoundingBox
	{
		return new GBoundingBox(this.x, this.y, this.x, this.y);
	}


	public toString(): string
	{
		return this.x.toFixed(1) + ',' + this.y.toFixed(1) + (this.ref ? ' + ' + this.ref.getAbsTransformation().toString() : '');
	}

	public toCss(): string
	{
		return this.x.toFixed(1) + ',' + this.y.toFixed(1);
	}


	public add(p: GPoint): GPoint
	{
		return new GPoint(this.x + p.x, this.y + p.y, this.ref);
	}


	public sub(p: GPoint): GPoint
	{
		return new GPoint(this.x - p.x, this.y - p.y, this.ref);
	}

}

