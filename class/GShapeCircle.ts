/*
 * Copyright (c) 2017, Josef Kufner  <josef@kufner.cz>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import GNode from './GNode';
import GPoint from './GPoint';
import GShape from './GShape';
import GSize from "./GSize";
import GSvg from './GSvg';
import GVector from './GVector';

export default class GShapeCircle extends GShape
{

	public renderElements(svg: GSvg, node: GNode): any[]
	{
		let d = node.getSize().width;
		return [
			svg.circle({
				'r': d / 2,
				'stroke-width': this.strokeWidth,
				'stroke': node.attr('color'),
				'fill': node.attr('fill'),
			}),
			this.renderLabel(svg, node),
		];
	}


	public getConnectorVector(node: GNode, sourcePoint: GPoint, connectorName?: string): GVector
	{
		let width = node.getSize().width;
		return new GVector(sourcePoint, node.getPosition(), node.getPosition().ref).addToSize(- width / 2);
	}


	public getMinSize(node: GNode): GSize
	{
		return new GSize(this.getLabelSize(node).getLonger());
	}

}

