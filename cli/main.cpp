/*
 * Copyright (c) 2017  Josef Kufner <josef@kufner.cz>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#if ENABLE_DEBUGGER
#include <QApplication>
#define QAPPLICATION QApplication
#else
#include <QCoreApplication>
#define QAPPLICATION QCoreApplication
#endif

#include <QCommandLineParser>

#include "GrafovatkoWrapper.h"

int main(int argc, char **argv)
{
	QAPPLICATION app (argc, argv);
	Q_INIT_RESOURCE(grafovatko);
	app.setApplicationName("Grafovatko");
#ifdef GIT_VERSION
	app.setApplicationVersion(GIT_VERSION);
#endif

	QCommandLineParser parser;
	parser.setApplicationDescription(app.tr(
			"Grafovatko is a graph visualization software — from a graph\n"
			"specification renders a beautiful SVG diagram."));
	parser.addHelpOption();
	parser.addVersionOption();
	parser.addPositionalArgument("input-file", app.tr("JSON file with graph definition."));

	parser.addOptions({
			{{"o", "output-file"},
			app.tr("Where to save the rendered diagram."),
			app.tr("file") }
			});

	parser.process(app);

	GrafovatkoWrapper gw;
	gw.setObjectName("cliWrapper");

	// TODO: Load file & set output
	
	gw.render();

	return 0;
	//return app.exec();
}


