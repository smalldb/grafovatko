/*
 * Copyright (c) 2017  Josef Kufner <josef@kufner.cz>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <QQmlEngine>
#include <QJSValue>
#include <QFile>
#include <QTextStream>
#include <QDebug>

#if ENABLE_DEBUGGER
#include <QScriptEngineDebugger>
#endif

#include "GrafovatkoWrapper.h"


GrafovatkoWrapper::GrafovatkoWrapper(QObject *parent)
	: QObject(parent)
{
	engine = new QQmlEngine(this);

#ifdef GIT_VERSION
	engine->globalObject().setProperty("cliWrapperVersion", GIT_VERSION);
#endif

#if ENABLE_DEBUGGER
	debugger = new QScriptEngineDebugger(this);
	debugger->attachTo(engine);
	debugger->setAutoShowStandardWindow(true);
#endif
}


GrafovatkoWrapper::~GrafovatkoWrapper()
{
}


void GrafovatkoWrapper::loadScripts()
{
	QString fileName(":/grafovatko.js");
	QFile scriptFile(fileName);
	scriptFile.open(QIODevice::ReadOnly);
	QTextStream stream(&scriptFile);
	QString contents = stream.readAll();
	scriptFile.close();

	engine->evaluate("console.log(cliWrapperVersion)");
	qDebug() << engine->evaluate(contents, fileName).toString();
	engine->evaluate("console.log('G:', G)");
}


int GrafovatkoWrapper::loadFile(const QString inputFilename)
{
	Q_UNUSED(inputFilename);
	return 0;
}


void GrafovatkoWrapper::setOutputFormat(const QString format)
{
	Q_UNUSED(format);
}


void GrafovatkoWrapper::setOutputFile(const QString filename)
{
	Q_UNUSED(filename);
}


int GrafovatkoWrapper::render()
{
	loadScripts();

	//qDebug() << engine->evaluate("cliWrapperVersion").toString();
	qDebug() << engine->evaluate("G").toString();

	return 0;
}

