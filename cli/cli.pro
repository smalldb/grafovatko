TEMPLATE = app
TARGET = grafovatko

QT = core qml

QT += gui widgets scripttools
DEFINES += ENABLE_DEBUGGER=0

GIT_VERSION = $$system(git describe --always --tags)
DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"

SOURCES += main.cpp
SOURCES += GrafovatkoWrapper.cpp
HEADERS += GrafovatkoWrapper.h
RESOURCES += grafovatko.qrc

grafovatko_js.target = "../build/grafovatko.js"
grafovatko_js.commands = "make -C .. build"

PRE_TARGETDEPS += "../build/grafovatko.js"
QMAKE_EXTRA_TARGETS += grafovatko_js

OBJECTS_DIR = .build
MOC_DIR = .build
RCC_DIR = .build
UI_DIR = .build

