/*
 * x.h
 * 
 * Copyright (c) 2017  Josef Kufner <josef@kufner.cz>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef GRAFOVATKO_WRAPPER_H
#define GRAFOVATKO_WRAPPER_H

#include <QObject>
#include <QString>

class QQmlEngine;
class QScriptEngineDebugger;

class GrafovatkoWrapper : public QObject
{
	Q_OBJECT

	public:
		GrafovatkoWrapper(QObject *parent = NULL);
		~GrafovatkoWrapper();

		int loadFile(const QString inputFilename);
		void setOutputFormat(const QString format);
		void setOutputFile(const QString filename);
		int render();

	private:
		QQmlEngine *engine;
#if ENABLE_DEBUGGER
		QScriptEngineDebugger *debugger;
#endif

		void loadScripts();
};

#endif

